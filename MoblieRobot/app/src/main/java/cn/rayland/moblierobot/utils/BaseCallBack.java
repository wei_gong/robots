package cn.rayland.moblierobot.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import cn.rayland.moblierobot.App;
import cn.rayland.moblierobot.R;

/**
 * Created by jinbangzhu on 7/10/15.
 */
public abstract class BaseCallBack implements Callback {
    static final String TAG = BaseCallBack.class.getSimpleName();
    private Handler handler = new Handler(Looper.getMainLooper());

    @Override
    public void onFailure(Request request, IOException e) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                ToastUtils.showShort(App.getContext(), App.getContext().getString(R.string.please_check_network));
                onFinish();
            }
        });
        Log.e(TAG, e.getLocalizedMessage());
    }

    @Override
    public void onResponse(Response response) throws IOException {
        if (null == response) {
            handlerError(null);
        } else {
            final String responseStr = response.body().string();
            if (!response.isSuccessful()) {
                handlerError(response);
            } else {
                if (!TextUtils.isEmpty(responseStr) || !responseStr.contains("error"))
                    onBizSuccessBackground(responseStr);

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (TextUtils.isEmpty(responseStr) || responseStr.contains("error"))
                            onBizFailure(responseStr);
                        else
                            onBizSuccess(responseStr);
                        onFinish();
                    }
                });
            }

            Log.d(TAG, "response body = " + responseStr);
        }


    }

    private void handlerError(final Response response) {
        String err = "ErrCode = %d, ErrMessage=%s";
        if (null != response) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    ToastUtils.showLong(App.getContext(), String.format(App.getContext().getString(R.string.server_encountered_some_trouble), response.code()));
                }
            });
            Log.e(TAG, String.format(err, response.code(), response.message()));
        } else {
            Log.e(TAG, "empty!!");
        }
    }


    abstract public void onBizFailure(String response);

    abstract public void onBizSuccess(String response);

    abstract public void onFinish();

    public void onBizSuccessBackground(String response) {
    }
}
