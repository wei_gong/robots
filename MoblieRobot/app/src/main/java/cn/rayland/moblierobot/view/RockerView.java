package cn.rayland.moblierobot.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.LinkedList;

/**
 * 方向摇杆View
 * Created by gw on 2016/2/16.
 */
public class RockerView extends View implements View.OnTouchListener {
    private int mWidth;
    private int mDir = CENTER;
    private int mLastDir = CENTER;
    private float x=0,y=0;
    private Paint mCenterPaint;
    private Paint mBackgroundPaint;
    private Paint mTrianglePaint;
    private static final int BACKGROUND_COLOR = 0XFF4e88d4;
    private static final int CENTER_COLOR = 0XFFb2b2b2;
    private boolean mTouched;
    private OnDirChangeListener onDirChangeListener;
    public static final int CENTER = 0;
    public static final int UP = 1;
    public static final int DOWN = 2;
    public static final int LEFT = 3;
    public static final int RIGHT = 4;
    private static int[] mTricky = new int[]{UP, RIGHT, DOWN, LEFT, UP, LEFT, DOWN, RIGHT, UP};
    private LinkedList<Integer> mActions;

    public RockerView(Context context) {
        this(context, null);
    }

    public RockerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RockerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setOnTouchListener(this);
        initActions();
    }

    private void initBackgroundPaint() {
        if(mBackgroundPaint == null){
            mBackgroundPaint = new Paint();
        }
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setColor(BACKGROUND_COLOR);
        mBackgroundPaint.setAlpha(190);
        mBackgroundPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mBackgroundPaint.setStrokeWidth(2.0f);
    }

    private void initCenterPaint() {
        if(mCenterPaint == null){
            mCenterPaint = new Paint();
        }
        mCenterPaint.setAntiAlias(true);
        mCenterPaint.setColor(CENTER_COLOR);
        mBackgroundPaint.setAlpha(190);
        mCenterPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mCenterPaint.setStrokeWidth(1.5f);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mWidth = Math.min(getMeasuredWidth(), getMeasuredHeight());
        setMeasuredDimension(mWidth, mWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawBackGroundCircle(canvas);
        drawCenterCircle(canvas);
        drawTriangle(canvas);
    }

    private void drawBackGroundCircle(Canvas canvas) {
        initBackgroundPaint();
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 2, mBackgroundPaint);
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 4, mBackgroundPaint);

        mBackgroundPaint.setColor(0XFFFFFFFF);
        mBackgroundPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 4 - 2, mBackgroundPaint);
    }

    private void drawCenterCircle(Canvas canvas) {
        initCenterPaint();
        if(mTouched){
            mCenterPaint.setAlpha(220);
            canvas.drawCircle(x, y, mWidth / 6, mCenterPaint);
        }else{
            mCenterPaint.setAlpha(190);
            canvas.drawCircle(mWidth / 2, mWidth / 2, mWidth / 6, mCenterPaint);
        }
    }

    private void initTrianglePaint() {
        if(mTrianglePaint == null){
            mTrianglePaint = new Paint();
        }
        mTrianglePaint.setAntiAlias(true);
        mTrianglePaint.setColor(0XFFFFFFFF);
        mTrianglePaint.setStyle(Paint.Style.FILL);
    }

    private void drawTriangle(Canvas canvas) {
        Path path = new Path();

        path.moveTo(mWidth / 2, mWidth/16);
        path.lineTo(mWidth * 4 / 9, mWidth / 6);
        path.lineTo(mWidth * 5 / 9, mWidth / 6);
        path.close();
        initTrianglePaint();
        if(mDir == UP){
            mTrianglePaint.setColor(0XFF2B2B2B);
        }
        canvas.drawPath(path, mTrianglePaint);

        Path path1 = new Path();
        path1.moveTo(mWidth / 2, mWidth * 15 / 16);
        path1.lineTo(mWidth * 4 / 9, mWidth * 5 / 6);
        path1.lineTo(mWidth * 5 / 9, mWidth * 5/6);
        path1.close();
        initTrianglePaint();
        if(mDir == DOWN){
            mTrianglePaint.setColor(0XFF2B2B2B);
        }
        canvas.drawPath(path1, mTrianglePaint);

        Path path2 = new Path();
        path2.moveTo(mWidth / 16, mWidth / 2);
        path2.lineTo(mWidth / 6, mWidth * 4 / 9);
        path2.lineTo(mWidth / 6, mWidth * 5/9);
        path2.close();
        initTrianglePaint();
        if(mDir == LEFT){
            mTrianglePaint.setColor(0XFF2B2B2B);
        }
        canvas.drawPath(path2,mTrianglePaint);

        Path path3 = new Path();
        path3.moveTo(mWidth *15 / 16, mWidth / 2);
        path3.lineTo(mWidth *5/6, mWidth * 4/9);
        path3.lineTo(mWidth *5/ 6, mWidth * 5/9);
        path3.close();
        initTrianglePaint();
        if(mDir == RIGHT){
            mTrianglePaint.setColor(0XFF2B2B2B);
        }
        canvas.drawPath(path3,mTrianglePaint);

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        x = event.getX();
        y = event.getY();
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                if(!ifInCircle(x, y)){
                    return true;
                }
                mTouched = true;
                checkDir(x, y);
                break;

            case MotionEvent.ACTION_MOVE:
                checkDir(x, y);
                break;

            case MotionEvent.ACTION_UP:
                mTouched = false;
//                mDir = CENTER;
                break;
        }
        invalidate();
        if(mDir != mLastDir && onDirChangeListener!=null){
            onDirChangeListener.onDirChange(mLastDir, mDir);
            if(checkTricky()){
                onDirChangeListener.onTricky();
            }
        }else if (mDir == mLastDir && onDirChangeListener!=null){
            onDirChangeListener.onTouch(mDir);
        }
        mLastDir = mDir;
        return true;
    }

    /**
     * 检测是否触发特殊操作
     */
    private boolean checkTricky() {
        if(mDir == CENTER){
            initActions();
            return false;
        }
        mActions.add(mActions.size(),mDir);
        mActions.removeFirst();
        for(int i=0;i<mActions.size();i++){
            if(mActions.get(i) != mTricky[i]){
                return false;
            }
        }
        return true;
    }

    /**
     * 判断一个点是否在圆内
     * @param x
     * @param y
     */
    private boolean ifInCircle(float x, float y) {
        if(x<=mWidth && x>=0 && y<=mWidth && y>=0){
            return true;
        }
        return false;
    }

    /**
     * 根据偏转角度判断摇杆方向
     * @param x
     * @param y
     */
    private void checkDir(float x, float y){
        float center_x = mWidth/2;
        float center_y = mWidth/2;
        float dx = Math.abs(x - center_x);
        float dy = Math.abs(y - center_y);
        if(dx>mWidth / 12 || dy>mWidth / 12 ){//中心圆不在中央区域
            int angle = getAngle(x, y);
            if(angle >= 45 && angle < 135){
                mDir = UP;
            }else if(angle >= 135 && angle < 225){
                mDir = LEFT;
            }
            else if(angle >= 225 && angle < 315){
                mDir = DOWN;
            }
            else if((angle >= 0 && angle < 45) || (angle >= 315 && angle <= 360)){
                mDir = RIGHT;
            }
            else{
                mDir = CENTER;
            }
        }else{
            mDir = CENTER;
        }
    }

    /**
     * 获取偏转角度
     * @param x
     * @param y
     * @return
     */
    private int getAngle(float x, float y){
        float center_x = mWidth/2;
        float center_y = mWidth/2;
        float dx = x - center_x;
        float dy = y - center_y;

        int angle = Math.round((float)(Math.atan(dy/dx)/Math.PI*180));

        if(dx<0){
            angle = 180 - angle;
        }else if(dx>0 && dy<0){
            angle = -angle;
        }else if(dx>0 && dy>0){
            angle = 360 - angle;
        }
        return angle;
    }

    public interface OnDirChangeListener {
        void onDirChange(int oldDir, int dir);
        void onTricky();
        void onTouch(int dir);
    }

    public void setOnDirChangeListener(OnDirChangeListener listener){
        this.onDirChangeListener = listener;
    }

    /**
     * 初始化记录的操作
     */
    private void initActions(){
        if(mActions == null){
            mActions = new LinkedList<>();
        }
        mActions.clear();
        for(int i=0;i<mTricky.length;i++){
            mActions.add(-1);
        }
    }
}
