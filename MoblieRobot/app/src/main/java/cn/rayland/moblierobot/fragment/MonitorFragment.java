package cn.rayland.moblierobot.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import cn.rayland.moblierobot.R;
import cn.rayland.moblierobot.bean.RobotState;
import de.greenrobot.event.EventBus;

/**
 * Created by gwlab on 2016/5/14.
 */
public class MonitorFragment extends BaseFragment {
    @InjectView(R.id.tv_x)
    TextView tv_x;
    @InjectView(R.id.tv_y)
    TextView tv_y;
    @InjectView(R.id.tv_z)
    TextView tv_z;
    @InjectView(R.id.tv_leftspeed)
    TextView tv_leftspeed;
    @InjectView(R.id.tv_rightspeed)
    TextView tv_rightspeed;
    @InjectView(R.id.tv_leftturns)
    TextView tv_leftturns;
    @InjectView(R.id.tv_rightturns)
    TextView tv_rightturns;
    @InjectView(R.id.tv_yaw)
    TextView tv_yaw;
    @InjectView(R.id.tv_laser_1)
    TextView tv_laser1;
    @InjectView(R.id.tv_laser_2)
    TextView tv_laser2;
    @InjectView(R.id.tv_laser_3)
    TextView tv_laser3;
    @InjectView(R.id.tv_laser_4)
    TextView tv_laser4;
    @InjectView(R.id.tv_laser_5)
    TextView tv_laser5;
    @InjectView(R.id.tv_battery)
    TextView tv_battery;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_monitor, container, false);
        ButterKnife.inject(this, view);
        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(RobotState state) {
        if(state != null){
            tv_x.setText(String.valueOf(state.getX()));
            tv_y.setText(String.valueOf(state.getY()));
            tv_z.setText(String.valueOf(state.getZ()));
            tv_leftspeed.setText(String.valueOf(state.getLeftSpeed()));
            tv_rightspeed.setText(String.valueOf(state.getRightSpeed()));
            tv_leftturns.setText(String.valueOf(state.getLeftTurns()));
            tv_rightturns.setText(String.valueOf(state.getRightTurns()));
            tv_yaw.setText(String.valueOf(state.getYaw()));
            tv_laser1.setText(String.valueOf(state.getLaser1()));
            tv_laser2.setText(String.valueOf(state.getLaser2()));
            tv_laser3.setText(String.valueOf(state.getLaser3()));
            tv_laser4.setText(String.valueOf(state.getLaser4()));
            tv_laser5.setText(String.valueOf(state.getLaser5()));
            tv_battery.setText(String.valueOf(state.getBattery_low()));
        }
    }
}
