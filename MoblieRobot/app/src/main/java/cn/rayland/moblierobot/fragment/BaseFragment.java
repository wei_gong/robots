package cn.rayland.moblierobot.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;

/**
 * Created by gwlab on 2016/5/14.
 */
public class BaseFragment extends Fragment {
    public Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
