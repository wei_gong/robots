package cn.rayland.moblierobot.bean;

public class RobotState {
	private float x;
	private float y;
	private float z;
	private float leftSpeed;
	private float rightSpeed;
	private float leftTurns;
	private float rightTurns;
	private float vx;
	private float vy;
	private float vz;
	private float pitch;
	private float roll;
	private float yaw;
	
	private int val;
	private boolean working;
	private boolean error;
	private float variable;
	
	private boolean battery_low;
	private boolean laser1;
	private boolean laser2;
	private boolean laser3;
	private boolean laser4;
	private boolean laser5;
	
	
	public boolean getBattery_low() {
		return battery_low;
	}
	public void setBattery_low(boolean battery_low) {
		this.battery_low = battery_low;
	}
	public boolean getLaser1() {
		return laser1;
	}
	public void setLaser1(boolean laser1) {
		this.laser1 = laser1;
	}
	public boolean getLaser2() {
		return laser2;
	}
	public void setLaser2(boolean laser2) {
		this.laser2 = laser2;
	}
	public boolean getLaser3() {
		return laser3;
	}
	public void setLaser3(boolean laser3) {
		this.laser3 = laser3;
	}
	public boolean getLaser4() {
		return laser4;
	}
	public void setLaser4(boolean laser4) {
		this.laser4 = laser4;
	}
	public boolean getLaser5() {
		return laser5;
	}
	public void setLaser5(boolean laser5) {
		this.laser5 = laser5;
	}
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		this.z = z;
	}
	public float getLeftSpeed() {
		return leftSpeed;
	}
	public void setLeftSpeed(float leftSpeed) {
		this.leftSpeed = leftSpeed;
	}
	public float getRightSpeed() {
		return rightSpeed;
	}
	public void setRightSpeed(float rightSpeed) {
		this.rightSpeed = rightSpeed;
	}
	public float getLeftTurns() {
		return leftTurns;
	}
	public void setLeftTurns(float leftTurns) {
		this.leftTurns = leftTurns;
	}
	public float getRightTurns() {
		return rightTurns;
	}
	public void setRightTurns(float rightTurns) {
		this.rightTurns = rightTurns;
	}
	public float getVx() {
		return vx;
	}
	public void setVx(float vx) {
		this.vx = vx;
	}
	public float getVy() {
		return vy;
	}
	public void setVy(float vy) {
		this.vy = vy;
	}
	public float getVz() {
		return vz;
	}
	public void setVz(float vz) {
		this.vz = vz;
	}
	public float getPitch() {
		return pitch;
	}
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
	public float getRoll() {
		return roll;
	}
	public void setRoll(float roll) {
		this.roll = roll;
	}
	public float getYaw() {
		return yaw;
	}
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	public int getVal() {
		return val;
	}
	public void setVal(int val) {
		this.val = val;
	}
	public boolean isWorking() {
		return working;
	}
	public void setWorking(boolean working) {
		this.working = working;
	}
	public boolean isError() {
		return error;
	}
	public void setError(boolean error) {
		this.error = error;
	}
	public float getVariable() {
		return variable;
	}
	public void setVariable(float variable) {
		this.variable = variable;
	}
	
	
	
	
	

}
