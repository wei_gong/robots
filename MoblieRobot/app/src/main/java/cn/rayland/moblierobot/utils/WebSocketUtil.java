package cn.rayland.moblierobot.utils;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.alibaba.fastjson.JSON;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import cn.rayland.moblierobot.App;
import cn.rayland.moblierobot.R;
import cn.rayland.moblierobot.bean.WBSocketMsg;
import de.greenrobot.event.EventBus;

/**
 * Created by jinbangzhu on 7/12/15.
 */
public class WebSocketUtil {
    public static final String START_GET_LOCATION = "start_get_location";
    public static final String STOP_GET_LOCATION = "stop_get_location";
    public static final String STOP_EXECUTE = "stop_execute";
    private static final String TAG = WebSocketUtil.class.getSimpleName();
    private static WebSocketClient mWebSocketClient;
    public static String localImei = null, remoteImei = null;

    static {
        localImei = getImei();
    }

    private static void initWebSocketClient(){
        try {
            URI uri = new URI("ws://182.92.84.88/slicer/api/ws?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjc0fQ.hoUTQRULE2Q5jPs6Iyl--F47xS1hqHzjWUJWtp-DLW4");
            mWebSocketClient = new WebSocketClient(uri, new Draft_17()) {
                @Override
                public void onOpen(ServerHandshake handshakedata) {
                    Log.i(TAG, "WebSocket connected");
                    WBSocketMsg wbSocketMsg = new WBSocketMsg();
                    wbSocketMsg.setCmd("connect");
                    wbSocketMsg.setImei(localImei);
                    wbSocketMsg.setName(getName());
                    String sendMsg = JSON.toJSONString(wbSocketMsg);
                    Log.i("", "Register as " + wbSocketMsg.getName() + " | " + wbSocketMsg.getImei());
                    mWebSocketClient.send(sendMsg);
                }

                @Override
                public void onMessage(String message) {
                    try {
                        WBSocketMsg msg = JSON.parseObject(message, WBSocketMsg.class);
                        Log.i(TAG, "Receive Message = " + message);
                        EventBus.getDefault().post(msg);
                    }catch (Exception e){
                        Log.i(TAG, "Register List = " + message);
                    }
                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    Log.i(TAG, "WebSocket closed");
                }

                @Override
                public void onError(Exception ex) {
                    Log.e(TAG, "WebSocket Errored");
                }
            };
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void open() {
        if(!isOpen()){
            initWebSocketClient();
            mWebSocketClient.connect();
        }
    }

    public static void close() {
        if(isOpen()){
            mWebSocketClient.close();
            mWebSocketClient = null;
        }
    }

    public static boolean isOpen() {
        return mWebSocketClient!=null && mWebSocketClient.getConnection().isOpen();
    }

    public static void sendMessage(WBSocketMsg msg){
        if(isOpen()){
            mWebSocketClient.send(JSON.toJSONString(msg));
            Log.i(TAG, "Send Message = " + JSON.toJSONString(msg));
        }
    }

    public static void sendMessage(String text) {
        if (remoteImei == null) {
            ToastUtils.showShort(App.getContext(), App.getContext().getString(R.string.please_connect_device));
            return;
        }
        WBSocketMsg msg = new WBSocketMsg();
        msg.setCmd("send");
        msg.setData(text);
        msg.setImei(remoteImei);
        msg.setName(getName());
        sendMessage(msg);
    }

    public static void sendMessage(String imei, String text) {
        if (imei == null) {
            return;
        }
        WBSocketMsg msg = new WBSocketMsg();
        msg.setCmd("send");
        msg.setData(text);
        msg.setImei(imei);
        msg.setName(getName());
        sendMessage(msg);
    }



    public static String getImei() {
        final TelephonyManager tm = (TelephonyManager) App.getContext().getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(App.getContext().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        return deviceUuid.toString();
    }

    public static String getName(){
        return Build.MODEL;
    }
}

