package cn.rayland.moblierobot.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cn.rayland.moblierobot.R;
import cn.rayland.moblierobot.bean.Device;
import cn.rayland.moblierobot.recyclerview.CommonAdapter;
import cn.rayland.moblierobot.recyclerview.ViewHolder;
import cn.rayland.moblierobot.utils.WebSocketUtil;


/**
 * Created by gw on 2016/5/11.
 */
public class DeviceListAdapter extends CommonAdapter<Device> {

    public DeviceListAdapter(Context context, final int layoutId) {
        super(context, layoutId, new ArrayList<Device>(0));
    }

    @Override
    public void convert(ViewHolder holder, final Device device) {
        ImageView deviceType = holder.getView(R.id.iv_device_type);
        TextView deviceName = holder.getView(R.id.tv_device_name);
        TextView deviceState = holder.getView(R.id.tv_device_state);
        deviceName.setText(device.getName());

    }

}
