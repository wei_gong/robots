package cn.rayland.moblierobot;

import android.app.Application;
import android.content.Context;


/**
 * Created by jinbangzhu on 6/23/15.
 */
public class App extends Application {
    private static App app = null;

    @Override
    public void onCreate() {
        super.onCreate();
        if(app == null){
            app = this;
        }
    }

public static Context getContext(){
    return app.getApplicationContext();
}
}
