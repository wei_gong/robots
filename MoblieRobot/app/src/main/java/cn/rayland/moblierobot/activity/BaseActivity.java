package cn.rayland.moblierobot.activity;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import cn.rayland.moblierobot.R;


/**
 * Created by gw on 2016/2/3.
 */
public class BaseActivity extends AppCompatActivity {
    private View view;
    private AlertDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //避免输入框遮挡显示内容
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    /**
     * 初始化toolbar
     * @param title
     */
    public void initToolBar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onOptionsItemSelected(item);
            }
        });
    }

    public void showProgressDialog() {
        if (null == dialog) {
            view = LayoutInflater.from(this).inflate(R.layout.progress_dialog, null);
            dialog = new AlertDialog.Builder(this).setView(view).create();
        }
        ((TextView)view.findViewById(R.id.id_progressText)).setText(getString(R.string.loading));
        dialog.setCanceledOnTouchOutside(false);
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public void showProgressDialog(String content) {
        if (null == dialog) {
            view = LayoutInflater.from(this).inflate(R.layout.progress_dialog, null);
            dialog = new AlertDialog.Builder(this).setView(view).create();
        }
        TextView textView = (TextView)view.findViewById(R.id.id_progressText);
        textView.setText(content);
        dialog.setCanceledOnTouchOutside(false);
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }


    public void changeDialogContent(String content) {
        if (dialog!=null && view!=null && dialog.isShowing()) {
            ((TextView)view.findViewById(R.id.id_progressText)).setText(content);
        }
    }

    public void hideProgressDialog() {
        if (null != dialog && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

}
