package cn.rayland.moblierobot.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import cn.rayland.moblierobot.R;
import cn.rayland.moblierobot.utils.ToastUtils;
import cn.rayland.moblierobot.utils.WebSocketUtil;
import cn.rayland.moblierobot.view.LineChartView;


/**
 * Created by gwlab on 2016/5/14.
 */
public class DebugFragment extends BaseFragment{

    @InjectView(R.id.et_left_number_turns)
    EditText et_leftNumberTurns;
    @InjectView(R.id.et_right_number_turns)
    EditText et_rightNumberTurns;
    @InjectView(R.id.et_speed)
    EditText et_speed;

    @InjectView(R.id.et_yaw)
    EditText etYaw;
    @InjectView(R.id.et_yaw_threshold)
    EditText yawThreshold;
    @InjectView(R.id.et_distance_threshold)
    EditText distanceThreshold;
    @InjectView(R.id.et_forward_distance)
    EditText forwardDistance;
    @InjectView(R.id.et_turn_speed)
    EditText turnSpeed;
    @InjectView(R.id.et_forward_speed)
    EditText forwardSpeed;
    private LineChartView chartView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug, container, false);
        ButterKnife.inject(this, view);
        chartView = (LineChartView) getActivity().findViewById(R.id.LineChartView);
        return view;
    }

    @OnClick({R.id.btn_go, R.id.btn_stop_go, R.id.btn_pilot, R.id.btn_stop_pilot})
    void onClick(View view){
        switch (view.getId()){
            case R.id.btn_go:
                sendTask();
                break;
            case R.id.btn_pilot:
                sendPilotTask();
                break;
            case R.id.btn_stop_go:
            case R.id.btn_stop_pilot:
                WebSocketUtil.sendMessage(WebSocketUtil.STOP_EXECUTE);
                break;
        }
    }

    private void sendTask() {
        String left = et_leftNumberTurns.getText().toString().trim();
        String right = et_rightNumberTurns.getText().toString().trim();
        String speed = et_speed.getText().toString().trim();
        if(left.equals("")){
           ToastUtils.showShort(context, "请填写左轮圈数");
            et_leftNumberTurns.requestFocus();
            return;
        }
        if(right.equals("")){
            ToastUtils.showShort(context, "请填写右轮圈数");
            et_rightNumberTurns.requestFocus();
            return;
        }
        if(speed.equals("")){
            ToastUtils.showShort(context, "请填写速度");
            et_speed.requestFocus();
            return;
        }
        String gcodeStr = "G6 X" + left + " Y" + right + " S" + speed;
        if (WebSocketUtil.remoteImei == null) {
            ToastUtils.showShort(getContext(), getString(R.string.please_content_machine));
        } else {
            ToastUtils.showShort(getContext(), gcodeStr);
            WebSocketUtil.sendMessage(gcodeStr);
        }
    }

    private void sendPilotTask() {
        float[] target = chartView.getTarget();
        String yaw = etYaw.getText().toString().trim();
        String yaw_threshold = yawThreshold.getText().toString().trim();
        String distance_threshold = distanceThreshold.getText().toString().trim();
        String forward_distance = forwardDistance.getText().toString().trim();
        String turn_speed = turnSpeed.getText().toString().trim();
        String forward_speed = forwardSpeed.getText().toString().trim();
        if(yaw.equals("")){
            ToastUtils.showShort(context, "请填写朝向角");
            etYaw.requestFocus();
            return;
        }
        if(yaw_threshold.equals("")){
            ToastUtils.showShort(context, "请填写角度偏差");
            yawThreshold.requestFocus();
            return;
        }
        if(distance_threshold.equals("")){
            ToastUtils.showShort(context, "请填写距离偏差");
            distanceThreshold.requestFocus();
            return;
        }
        if(forward_distance.equals("")){
            ToastUtils.showShort(context, "请填写直行距离");
            forwardDistance.requestFocus();
            return;
        }
        if(forward_speed.equals("")){
            ToastUtils.showShort(context, "请填写直行速度");
            forwardSpeed.requestFocus();
            return;
        }
        if(turn_speed.equals("")){
            ToastUtils.showShort(context, "请填写转向速度");
            turnSpeed.requestFocus();
            return;
        }
        String gcodeStr = "G9 X"+target[0]+" Y"+target[1]+" Z0 W"+yaw+" E"+yaw_threshold+" F"+forward_distance+" R"+turn_speed+" S"+forward_speed+" D"+distance_threshold;
        if (WebSocketUtil.remoteImei == null) {
            ToastUtils.showShort(getContext(), getString(R.string.please_content_machine));
        } else {
            ToastUtils.showShort(getContext(), gcodeStr);
            WebSocketUtil.sendMessage(gcodeStr);
        }
    }
}
