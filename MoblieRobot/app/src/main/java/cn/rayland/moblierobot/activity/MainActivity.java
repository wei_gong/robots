package cn.rayland.moblierobot.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.alibaba.fastjson.JSON;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import cn.rayland.moblierobot.R;
import cn.rayland.moblierobot.adapter.DeviceListAdapter;
import cn.rayland.moblierobot.bean.Device;
import cn.rayland.moblierobot.bean.RobotState;
import cn.rayland.moblierobot.bean.WBSocketMsg;
import cn.rayland.moblierobot.fragment.BaseFragment;
import cn.rayland.moblierobot.fragment.DebugFragment;
import cn.rayland.moblierobot.fragment.MonitorFragment;
import cn.rayland.moblierobot.recyclerview.DividerItemDecoration;
import cn.rayland.moblierobot.recyclerview.OnItemClickListener;
import cn.rayland.moblierobot.utils.BaseCallBack;
import cn.rayland.moblierobot.utils.HttpUtils;
import cn.rayland.moblierobot.utils.WebSocketUtil;
import cn.rayland.moblierobot.view.LineChartView;
import de.greenrobot.event.EventBus;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.drawer_layout)
    DrawerLayout drawer;
    @InjectView(R.id.nav_view)
    NavigationView navigationView;
    @InjectView(R.id.LineChartView)
    LineChartView chartView;
    private DebugFragment debugFragment;
    private MonitorFragment monitorFragment;
    private int mCurrentFragmentTag;
    private BaseFragment curInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        navigationView.setNavigationItemSelectedListener(this);
        setShowFragment(R.id.status_monitor);
        WebSocketUtil.open();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_main){
            if(drawer.isDrawerOpen(GravityCompat.END)){
                drawer.closeDrawer(GravityCompat.END);
            }else{
                drawer.openDrawer(GravityCompat.END);
            }
        }else if(item.getItemId() == R.id.menu_connect){
           showChooseDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showChooseDialog() {
        View contentView = LayoutInflater.from(this).inflate(R.layout.popup_device_layout, null);
        final AlertDialog dialog = new AlertDialog.Builder(this).setView(contentView).create();
        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        params.width = getResources().getDisplayMetrics().widthPixels * 7/10;
        params.height = getResources().getDisplayMetrics().heightPixels * 3/5 ;
        dialog.getWindow().setAttributes(params);
        dialog.show();

        final DeviceListAdapter adapter = new DeviceListAdapter(this, R.layout.adapter_device_item);
        RecyclerView recyclerView = (RecyclerView) contentView.findViewById(R.id.recycler_view);
        final SwipeRefreshLayout pullRefresh = (SwipeRefreshLayout) contentView.findViewById(R.id.pullRefresh);
        pullRefresh.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorPrimary),
                ContextCompat.getColor(this, R.color.colorAccent),
                ContextCompat.getColor(this, R.color.colorPrimaryDark));
        pullRefresh.setProgressViewOffset(false, 0, (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources()
                        .getDisplayMetrics()));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new OnItemClickListener<Device>() {
            @Override
            public void onItemClick(ViewGroup parent, View view, Device device, int position) {
                WebSocketUtil.remoteImei = device.getImei();
                WebSocketUtil.sendMessage(WebSocketUtil.START_GET_LOCATION);
                dialog.dismiss();
            }

            @Override
            public boolean onItemLongClick(ViewGroup parent, View view, Device device, int position) {
                return false;
            }
        });

        pullRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDevices(pullRefresh, adapter);
            }
        });
        loadDevices(pullRefresh, adapter);
    }

    public void loadDevices(final SwipeRefreshLayout refreshLayout, final DeviceListAdapter adapter){
        refreshLayout.setRefreshing(true);
        HttpUtils.getInstance().get("http://182.92.84.88/slicer/api/devices", new BaseCallBack() {


                    @Override
                    public void onBizFailure(String response) {

                    }

                    @Override
                    public void onBizSuccess(String response) {
                        try {
                            List<Device> allDevices = JSON.parseArray(response, Device.class);
                            List<Device> printers = new ArrayList<>();
                            for(Device devcice : allDevices){
                                if(devcice.getImei().equals(WebSocketUtil.localImei)){
                                    continue;
                                }
                                printers.add(devcice);
                            }
                            adapter.setDatas(printers);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFinish() {
                        refreshLayout.setRefreshing(false);
                    }
                });
    }



    /**
     * 设置当前activity所展示的fragment
     *
     * @param tag 侧滑菜单项的id
     */
    private void setShowFragment(int tag) {
        if(mCurrentFragmentTag != 0 && tag == mCurrentFragmentTag){
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if(curInfoFragment != null){
            ft.hide(curInfoFragment);
        }
        BaseFragment fragment = (BaseFragment) fm.findFragmentByTag(String.valueOf(tag));
        if(fragment != null){
            ft.show(fragment);
        }else{
            switch (tag) {
                case R.id.debug:
                    if (null == debugFragment) {
                        debugFragment = new DebugFragment();
                    }
                    fragment = debugFragment;
                    break;
                case R.id.status_monitor:
                    if (null == monitorFragment) {
                        monitorFragment = new MonitorFragment();
                    }
                    fragment = monitorFragment;
                    break;
            }
            if (!fragment.isAdded()) {
                ft.add(R.id.container, fragment, String.valueOf(tag));
            } else {
                ft.show(fragment);
            }
        }
        curInfoFragment = fragment;
        mCurrentFragmentTag = tag;
        ft.commitAllowingStateLoss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        WebSocketUtil.sendMessage(WebSocketUtil.START_GET_LOCATION);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        WebSocketUtil.sendMessage(WebSocketUtil.STOP_GET_LOCATION);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WebSocketUtil.close();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        drawer.closeDrawer(GravityCompat.END);
        int id = item.getItemId();
        setShowFragment(id);
        return true;
    }

    public void onEventMainThread(final WBSocketMsg event) {
        String data = event.getData();
        if (data.startsWith("send_coordinat")) {
            String strs = data.substring(data.indexOf("send_coordinat") + 14);
            RobotState state = JSON.parseObject(strs, RobotState.class);
            chartView.setMove(state.getX(), state.getY(), state.getYaw());
            EventBus.getDefault().post(state);
        }
    }

}
