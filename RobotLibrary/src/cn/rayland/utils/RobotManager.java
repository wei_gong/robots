package cn.rayland.utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import cn.rayland.api.Gpx;
import cn.rayland.api.Robot;
import cn.rayland.bean.MachineTask;
import cn.rayland.bean.MachineTask.TaskCallback;
import cn.rayland.bean.RobotState;
import cn.rayland.bean.SeniorSetting;
import cn.rayland.commands.GcodeCommand;
import cn.rayland.commands.X3gCommand;
import cn.rayland.commands.QueryCommand;
import cn.rayland.library.stm32.IIC;
import android.content.Context;
import android.util.Log;

/**
 * Created by gw on 2016/3/2.
 */
public class RobotManager {
	private static final String TAG = RobotManager.class.getSimpleName();
	private static final int TOTAL_VAL = 255;
	private static RobotManager instance;
	private Context context;
	private IIC stm32;
	private CommandQueue queue;
	public volatile Robot robot;
	private volatile String customConfigPath;
	private QueryCommand queryCommand;

	public static RobotManager getInstance(Context context){
		if(instance == null){
			synchronized (RobotManager.class) {
				if(instance == null){
					instance = new RobotManager(context);
				}
			}
		}
		return instance;
	}

	private RobotManager(Context context){
		this.context = context;
		stm32 = IIC.getInstance();
		ZMQManager.init(this);
		queryCommand = new QueryCommand(new RobotState());
		queue = CommandQueue.create(queryCommand);
		initMachine();
	}

	/**
	 * set machine config according to customConfigPath
	 * if customConfigPath is null or not supported,use default config saved in assets
	 * when machine config changed, init gpx and send initX3g  
	 */
	public void initMachine() {
		try {
			InputStream fis = (customConfigPath == null) ? (context.getAssets()
					.open("machine.txt")) : (new FileInputStream(
							customConfigPath));
			String settings = IOUtils.readToString(fis);
			fis.close();
			List<SeniorSetting> seniorSettings = JsonUtils.parseSeniorSettings(settings);
			for (int i = 0; i < seniorSettings.size(); i++) {
				if (seniorSettings.get(i).isSelected()) {
					Robot robotNew = (Robot) seniorSettings.get(i).getSetting();

					if(robot == null || !robot.equals(robotNew)){
						robot = robotNew;
						Gpx.gpxInit(robot);
						byte[] initX3g = Gpx.getInitX3g(robot);
						X3gCommand command = new X3gCommand(initX3g, queryCommand, null);
						command.setStopWhenError(false);
						queue.addCommand(command);
					}

					break;
				}
				if (i == seniorSettings.size() - 1) {
					customConfigPath = null;
					initMachine();
					Log.e(TAG, "cause custom config not found,use default config");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			customConfigPath = null;
		}
	}

	/**
	 * get a RobotState which contains everything about the robot state
	 */
	public RobotState getRobotState(){
//		queryCommand.execute();
		return queryCommand.getContent();
	}

	/**
	 * get length of cached command queue
	 */
	public int getOrderQueueLength(){
		return TOTAL_VAL - getRobotState().getVal();
	}

	/**
	 * clear cached command queue
	 */
	public void clearOrderQueue(){
		CommandUtils.execReset();
	}

	/**
	 * if force false, stop executing,sending command
	 * if force true, stop executing,sending command, clear cached command queue
	 */
	public void stop(boolean force){
		if(queue == null){
			return;
		}
		queue.cancelLoop();
		queue.cancelCurrentCommand();
		queue.removeAllCommand();
		queue.startLoop();
		if(force){
			CommandUtils.execReset();
//			Gpx.gpxInit(machine);
			Log.d(TAG, "stop");
		}
	}
	
	public void cancel(){
		queue.cancelLoop();
		queue.removeAllCommand();
		queue.startLoop();
	}

	/**
	 * stop executing,sending command,memory current execute position
	 */
	public void pause(){
		//TODO
	}

	/**
	 * concume from last execut position
	 */
	public void resume(){
		//TODO
	}
	/**
	 * check either machine is in use or not
	 * if rest true, init gpx
	 * @return
	 */
	private boolean checkMachine() {
		boolean checkPass = true;
		if (stm32 == null || !stm32.haveDevice()) {
			checkPass = false;
			Log.e(TAG, "stm32 not found");
		}
//		if (getRobotState().isWorking()) {
//			checkPass = false;
//			Log.e(TAG, "stm32 is busy");
//		}
		initMachine();
//		if(reset){
//			Gpx.gpxInit(machine);
//		}
		return checkPass;
	}

	/**
	 * check if need to reset
	 */
	private void prepare(boolean reset) { 
		if (reset) {
			CommandUtils.execReset();
			Log.d(TAG, "stop");
		}
	}

	/**
	 * send gcode command to execute
	 * @param gcodeStr
	 * @param task
	 * @param reset
	 */
	public void sendTask(MachineTask<String> task, final boolean ifReset) {
		if (task == null || task.getContent() == null || queue == null) {
			return;
		}
		if (!checkMachine()){
			return;
		}
		prepare(ifReset);
		GcodeCommand command = new GcodeCommand(task.getContent(), queryCommand, task.getCallback());
		queue.addCommand(command);	
	}

	/**
     * @param task template:G9 X Y Z W E F R S
     * x target location's x coordinate
     * y    target location's y coordinate
     * z target location's z coordinate
     * w yaw value when target arrived
     * e allowed offset threshold about yaw, unit angle
     * e allowed offset threshold about distance, unit mm
     * f each go_forward distance,unit mm
     * r speed during trun action,unit mm
     * s speed during go_forward action,unit mm
     */
	public void sendPilotTask(final MachineTask<String> task, final float distance_threshold){
		final MachineTask<String> pilotTask = new MachineTask<String>();
		String gcodeString = task.getContent().toLowerCase();
		final float targetX = Float.parseFloat(gcodeString.substring(gcodeString.indexOf(" x")+2, gcodeString.indexOf(" y")));
		final float targetY = Float.parseFloat(gcodeString.substring(gcodeString.indexOf(" y")+2, gcodeString.indexOf(" z")));
//		final float yaw_threshold = Float.parseFloat(gcodeString.substring(gcodeString.indexOf(" e")+2, gcodeString.indexOf(" f")));
		pilotTask.setContent(task.getContent());
		pilotTask.setCallback(new TaskCallback() {
			float lastX,lastY,lastZ,yaw,nowX,nowY,nowZ,distance;
			public void onStart() {
				RobotState state = getRobotState();
				lastX = state.getX();
				lastY = state.getY();
				lastZ = state.getZ();
			}
			
			public void onWorking() {
				RobotState state = getRobotState();
				float x = state.getX();
				float y = state.getY();
				float distance = (float) Math.sqrt(Math.pow((targetX - x), 2) + Math.pow((targetY - y), 2));
				if(distance < distance_threshold){
					stop(true);
					Log.d(TAG, "Arrived");
					if(task.getCallback()!=null){
						task.getCallback().onFinished();
					}
				}
			}

			public void onError() {
				if(task.getCallback()!=null){
					task.getCallback().onError();
				}
			}

			public void onCancel() {
				if(task.getCallback()!=null){
					task.getCallback().onCancel();
				}
			}

			public void onFinished() {
				RobotState state = getRobotState();
				nowX = state.getX();
				nowY = state.getY();
				nowZ = state.getZ();
				yaw = state.getYaw();
//				yaw = (float) Math.atan2(nowY - lastY, nowX - lastX);
//				state.setYaw((float) (yaw/Math.PI*180));
				queue.addCommand(new GcodeCommand("G92 X"+nowX+" Y"+nowY+" Z"+nowZ+" A"+yaw*Math.PI/180, queryCommand, null));
				Log.d(TAG, "G92 X"+lastX+" Y"+lastY+" Z"+lastZ+" A"+yaw);
				
				distance = (float) Math.sqrt(Math.pow((targetX - nowX), 2) + Math.pow((targetY - nowY), 2));
				if(distance > distance_threshold){
					sendTask(pilotTask, true);
				}else{
					Log.d(TAG, "Arrived");
					if(task.getCallback()!=null){
						task.getCallback().onFinished();
					}
				}
			}
		});
		RobotState state = getRobotState();
		queue.addCommand(new GcodeCommand("G92 X"+state.getX()+" Y"+state.getY()+" Z"+state.getZ(), queryCommand, null));
		sendTask(pilotTask, true);
	}

	/**
	 * set custom machine config to adapter your own robot
	 * @param configFilePath
	 */
	public boolean setCustomMachineConfig (String configFilePath){
		if(checkConfig(configFilePath)){
			customConfigPath = configFilePath;
			return true;
		}
		return false;
	}
	/**
	 * check your custom machine config is supported or not
	 * @param configFilePath
	 */
	private boolean checkConfig(String configFilePath) {
		try {
			String settings = IOUtils.readToString(new FileInputStream(configFilePath));
			List<SeniorSetting> seniorSettings = JsonUtils.parseSeniorSettings(settings);
			for (int i=0;i<seniorSettings.size();i++) {
				if (seniorSettings.get(i).isSelected()) {
					break;
				}
				if(i == seniorSettings.size()-1){
					Log.e(TAG, "selected config not found");
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
//	/**
//	 * set ultrasound location system 's config
//	 * @return
//	 */
//	public boolean setUltraLocationConfig(UltraLocationConfig config){
//		if(config!=null){
//			StringBuilder builder = new StringBuilder();
//			
//			int num = config.getTransmitterNum();
//			if(num == 0){
//				return false;
//			}
//			builder.append("M220; " + FormatUtils.int32Toint16Asc(num) + " ");
//			
//			int[] imus = config.getImuOffset();
//			if(imus.length < 9){
//				return false;
//			}
//			for(int i : imus){	
//				builder.append(FormatUtils.int32Toint16Asc(i)+" ");
//			}
//			
//			List<Transmitter> transmitters = config.getTransmitters();
//			if(transmitters == null || transmitters.size() == 0){
//				return false;
//			}
//			for(Transmitter transmitter : transmitters){
//				builder.append(FormatUtils.int32Toint16Asc(transmitter.getId())+" ");
//				builder.append(FormatUtils.int32Toint16Asc(transmitter.getX())+" ");
//				builder.append(FormatUtils.int32Toint16Asc(transmitter.getY())+" ");
//				builder.append(FormatUtils.int32Toint16Asc(transmitter.getZ())+" ");
//				builder.append(FormatUtils.int32Toint16Asc(transmitter.getOffset())+" ");
//			}
//			GcodeCommand command = new GcodeCommand(builder.toString().trim(), queryCommand, null);
//			queue.addCommand(command);
//		}
//		return true;
//	}
//	
}
