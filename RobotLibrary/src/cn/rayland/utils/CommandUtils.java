package cn.rayland.utils;

import cn.rayland.library.stm32.IIC;

public class CommandUtils{
	private static IIC stm32 = IIC.getInstance();
	private static byte[] stop = new byte[]{1,0};
	private static byte[] reset = new byte[]{8,0};
	private static byte[] reboot = new byte[]{16,0};
	
	public static void execStop(){
		stm32.sendData(stop);
	}
	
	public static void execReset(){
		stm32.sendData(reset);
	}
	
	public static void execReboot(){
		stm32.sendData(reboot);
	}
	
}
