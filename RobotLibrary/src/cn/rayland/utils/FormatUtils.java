package cn.rayland.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class FormatUtils {

	public static short byte2short(byte[] bytes) {
		ByteBuffer buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
		return buffer.getShort(0);
	}
	
	
	public static float byte2float(final byte[] bytes) {
		ByteBuffer buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
		return buffer.getFloat(0);
	    }
	
	public static int byte2int(final byte[] bytes) {
		ByteBuffer buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
		return buffer.getInt(0);
    }
	
	public static String int32Toint16Asc(int i) {
		  byte[] array = ByteBuffer.allocate(4).putInt(i).array();
	        StringBuilder stringBuilder = new StringBuilder();
	        for(int j=array.length-1;j>1;j--){
	            stringBuilder.append(Integer.toHexString(array[j] & 0XFF)+" ");
	        }
//	        for(int j=2;j<array.length;j++){
//	            stringBuilder.append(Integer.toHexString(array[j] & 0XFF)+" ");
//	        }
	        return stringBuilder.toString().trim().toUpperCase();
    }
}
