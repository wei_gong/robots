package cn.rayland.utils;

import java.util.LinkedList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import android.util.Log;
import cn.rayland.commands.Command;

public class CommandQueue {
	private static final int CAPACITY = 5;
	private static CommandQueue queue;
	private LinkedBlockingQueue<Command> commandList;
	private volatile boolean runningLoop;
	private volatile Command currentCommand;
	private Command queryCommand;
	private Executor executor;
	
	public static CommandQueue create(Command queryCommand){
		if(queue == null){
			synchronized (CommandQueue.class) {
				if(queue == null){
					queue = new CommandQueue(queryCommand);
				}
			}
		}
		return queue;
	}
	
	private CommandQueue(Command queryCommand){
		commandList = new LinkedBlockingQueue<Command>(CAPACITY);
		executor = Executors.newSingleThreadExecutor();
		this.queryCommand = queryCommand;
		startLoop();
	}
	

	public void startLoop() {
		if(!runningLoop){
			runningLoop = true;
			executor.execute(loop);
		}
	}
	
	public void cancelLoop() {
		if(runningLoop){
			runningLoop = false;
		}
	}
	
	public void cancelCurrentCommand(){
		if(currentCommand!=null){
			currentCommand.cancel();
		}
	}
	
	public void removeCommand(Command command){
		if(command != null && commandList.contains(command)){
			try {
				commandList.take();
			} catch (InterruptedException e) {
			}
		}
	}
	
	public void removeAllCommand(){
		for(Command command : commandList){
			removeCommand(command);
		}
	}
	
	public void addCommand(LinkedList<Command> commands){
		if(commands != null && commands.size() > 0){
			for(Command command : commands){
				addCommand(command);
			}
		}
	}
	
	public void addCommand(Command command){
		if(command != null){
			try {
				commandList.put(command);
				Log.d("CommandQueue", "add command = "+command.getContent());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public int getSize(){
		return commandList.size();
	}
	
	private Runnable loop = new Runnable() {
		
		public void run() {
			while(runningLoop){
				if(commandList.isEmpty()){
					queryCommand.execute();
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}else{
					try {
						currentCommand = commandList.take();
						if(currentCommand!=null){
							currentCommand.execute();
							Log.i("CommandQueue", "finished command = " +currentCommand.getContent());
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	};
}
