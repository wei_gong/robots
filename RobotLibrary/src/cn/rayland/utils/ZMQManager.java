package cn.rayland.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;

import cn.rayland.bean.MachineTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

public class ZMQManager {
	
	private static Socket receiveUvSocket;
	private static Socket receiveMapSocket;
	private static Socket sendSocket;
	private static ZContext context;
	private static Thread receiveUvThread;
	private static Thread receiveMapThread;
	private static HandlerThread sendThread;
	private static Handler sendHandler;
	private static volatile boolean stopReceiving;
	private static RobotManager robotManager;
	
	public static void init(RobotManager manager){
		setEthIp();
		robotManager = manager;
		context = new ZContext();
		receiveUvSocket = context.createSocket(ZMQ.SUB);
		receiveMapSocket = context.createSocket(ZMQ.SUB);
		sendSocket = context.createSocket(ZMQ.PUB);
		receiveUvSocket.connect("tcp://192.168.199.3:5553");
		receiveMapSocket.connect("tcp://192.168.199.3:5555");
		sendSocket.bind("tcp://*:5554");
		receiveUvSocket.subscribe("".getBytes());
		receiveUvThread = new Thread(receiveUvTask);
		receiveUvThread.start();
		receiveMapSocket.subscribe("".getBytes());
		receiveMapThread = new Thread(receiveMapTask);
		receiveMapThread.start();
		sendThread = new HandlerThread("send");
		sendThread.start();
		sendHandler = new Handler(sendThread.getLooper());
	}
	
	private static void setEthIp() {
		DataOutputStream dos = null;
		Process process = null;
		try {
			process = Runtime.getRuntime().exec("ifconfig eth0 192.168.199.4 netmask 255.255.255.0\n");
			dos = new DataOutputStream(process.getOutputStream());
			dos.writeBytes("exit\n");
			dos.flush();
			process.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} finally {
			if(dos!=null){
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(process!=null){
				process.destroy();
			}
		}
	}

	private static Runnable receiveUvTask = new Runnable() {
		
		public void run() {
			Log.i("zmq", "Trying to receive" );
			try {
				while(!stopReceiving) {
					byte[] bytes = receiveUvSocket.recv();
					int u = ((bytes[0]&0xff)|(bytes[1]<<8));
					int v = ((bytes[2]&0xff)|(bytes[3]<<8));
					MachineTask<String> task = new MachineTask<String>();
					task.setContent("G7 X0 Y0 A"+u+" B"+v);
					Log.d("ZMQ_MANAGER", "receive command = "+"G7 X0 Y0 A"+u+" B"+v);
					robotManager.sendTask(task, false);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.i("zmq", "Stop Receiving" );
		}
	};
	
private static Runnable receiveMapTask = new Runnable() {
		
		public void run() {
			Log.i("zmq", "Trying to receive Map data" );
			try {
				while(!stopReceiving) {
					byte[] bytes = receiveMapSocket.recv();
					Log.i("zmq", "receive "+bytes.length+" bytes");
					for(int i=0;i<bytes.length;i++){
						Log.i("zmq", bytes[i]+"");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.i("zmq", "Stop Receiving" );
		}
	};
	
	public static void sendFeedback(final byte[] bytes){
		sendHandler.post(new Runnable() {
			public void run() {
				sendSocket.send(bytes, bytes.length);
			}
		});
	}
	
	public static void destory(){
		if(receiveUvThread!=null){
			stopReceiving = true;
			receiveUvThread.interrupt();
			receiveUvThread = null;
		}
		if(receiveMapThread!=null){
			stopReceiving = true;
			receiveMapThread.interrupt();
			receiveMapThread = null;
		}
		if(sendThread!=null){
			sendThread.quit();
			sendThread = null;
		}
		if(receiveUvSocket!=null){
			receiveUvSocket.close();
			receiveUvSocket = null;
		}
		if(receiveMapSocket!=null){
			receiveMapSocket.close();
			receiveMapSocket = null;
		}
		if(sendSocket!=null){
			sendSocket.close();
			sendSocket = null;
		}
		if(context!=null){
			context = null;
		}
	}
	
}
