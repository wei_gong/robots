package cn.rayland.utils;


import cn.rayland.bean.MachineTask;
import android.content.Context;

public class MachineController {
	private static MachineController instance;
	private RobotManager machineManager;
	private MachineTask<String> task = new MachineTask<String>();
	private static final String x_left = "G130 X80 Y80 Z80 A80 B80\r\n" +
	            "G1 X-3000 F1000";
	private static final String x_right = "G130 X80 Y80 Z80 A80 B80\r\n" +
	            "G1 X3000 F1000";
	private static final String y_forward = "G130 X80 Y80 Z80 A80 B80\r\n" +
	            "G1 Y3000 F1000";
	private static final String y_backward = "G130 X80 Y80 Z80 A80 B80\r\n" +
	            "G1 Y-3000 F1000";
	private static final String z_up = "G130 X80 Y80 Z80 A80 B80\r\n" +
	            "G1 Z-3000 F500";
	private static final String z_down = "G130 X80 Y80 Z80 A80 B80\r\n" +
	            "G1 Z3000 F500";
	private static final String in_filament = "M109 S220.0\r\n" +
	            "G130 X80 Y80 Z80 A80 B80\r\n" +
	            "G1 A6000.0 F300";
	private static final String out_filament = "M109 S220.0\r\n" +
	            "G130 X80 Y80 Z80 A80 B80\r\n" +
	            "G1 A-6000.0 F-300";
	
	public static MachineController getInstance(Context context){
		if(instance == null){
			instance = new MachineController(context);
		}
		return instance;
	}
	
	private MachineController(){}
	private MachineController(Context context){
		this.machineManager = RobotManager.getInstance(context);
	}

	public void xLeft() {
		task.setContent(x_left);
		machineManager.sendTask(task, true);
	}

	public void xRight() {
		task.setContent(x_right);
		machineManager.sendTask(task, true);
	}

	public void yForward() {
		task.setContent(y_forward);
		machineManager.sendTask(task, true);
	}

	public void yBackForward() {
		task.setContent(y_backward);
		machineManager.sendTask(task, true);
	}

	public void zUp() {
		task.setContent(z_up);
		machineManager.sendTask(task, true);
	}

	public void zDown() {
		task.setContent(z_down);
		machineManager.sendTask(task, true);
	}

	public void inFilament() {
		task.setContent(in_filament);
		machineManager.sendTask(task, true);
	}

	public void outFilament() {
		task.setContent(out_filament);
		machineManager.sendTask(task, true);
	}

	public void stop() {
		machineManager.stop(true);
	}
}
