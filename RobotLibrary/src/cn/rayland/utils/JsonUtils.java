package cn.rayland.utils;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import cn.rayland.api.IMU;
import cn.rayland.api.Machine;
import cn.rayland.api.Motor;
import cn.rayland.api.Robot;
import cn.rayland.api.ObstacleSensor;
import cn.rayland.api.Transmitter;
import cn.rayland.bean.SeniorSetting;

/**
 * Created by gw on 2015-08-11.
 */
public class JsonUtils {

	public static List<SeniorSetting> parseSeniorSettings(String jsonString) throws JSONException{
		
		JSONArray jsonArray = new JSONArray(jsonString);
		List<SeniorSetting> seniorSettings = new ArrayList<SeniorSetting>();
		int size = jsonArray.length();
		for(int i=0;i<size;i++){
			JSONObject seniorSettingJsonObject = jsonArray.getJSONObject(i);
			SeniorSetting seniorSetting = parseSeniorSetting(seniorSettingJsonObject.toString());
			seniorSettings.add(seniorSetting);
		}
		return seniorSettings;
	}
	
public static SeniorSetting parseSeniorSetting(String jsonString) throws JSONException{
		
			JSONObject seniorSettingJsonObject = new JSONObject(jsonString);
			SeniorSetting seniorSetting = new SeniorSetting();
			seniorSetting.setName(seniorSettingJsonObject.optString("name", "unknow"));
			seniorSetting.setSelected(seniorSettingJsonObject.optBoolean("selected", false));
			
			JSONObject machineJsonObject = seniorSettingJsonObject.getJSONObject("setting");
			Machine machine = null;
			switch (machineJsonObject.optInt("machine_type", 0)) {
			case 0:
				
				break;
			case 1:
				
				break;
			case 2:
	
				break;
			case 3:
	
				break;
			case 4:
				Robot robot = new Robot();
				JSONObject motorXJsonObject = machineJsonObject.optJSONObject("motor_x");
				Motor motorX = new Motor();
				motorX.pulse_per_round = motorXJsonObject==null ? 0 : motorXJsonObject.optInt("pulse_per_round", 6);
				motorX.reduction_gear_ratio = motorXJsonObject==null ? 0 : motorXJsonObject.optDouble("reduction_gear_ratio", 75);
				robot.motor_x = motorX;
				
				JSONObject motorYJsonObject = machineJsonObject.optJSONObject("motor_y");
				Motor motorY = new Motor();
				motorY.pulse_per_round = motorYJsonObject==null ? 0 : motorYJsonObject.optInt("pulse_per_round", 6);
				motorY.reduction_gear_ratio = motorYJsonObject==null ? 0 : motorYJsonObject.optDouble("reduction_gear_ratio", 75);
				robot.motor_y = motorY;
				
				JSONObject motorAJsonObject = machineJsonObject.optJSONObject("motor_a");
				Motor motorA = new Motor();
				motorA.pulse_per_round = motorAJsonObject==null ? 0 : motorAJsonObject.optInt("pulse_per_round", 6);
				motorA.reduction_gear_ratio = motorAJsonObject==null ? 0 : motorAJsonObject.optDouble("reduction_gear_ratio", 75);
				robot.motor_a = motorA;
				
				JSONObject motorBJsonObject = machineJsonObject.optJSONObject("motor_b");
				Motor motorB = new Motor();
				motorB.pulse_per_round = motorBJsonObject==null ? 0 : motorBJsonObject.optInt("pulse_per_round", 6);
				motorB.reduction_gear_ratio = motorBJsonObject==null ? 0 : motorBJsonObject.optDouble("reduction_gear_ratio", 75);
				robot.motor_b = motorB;
				
				robot.wheel_diameter = machineJsonObject.optDouble("wheel_diameter", 75);
				robot.axial_length = machineJsonObject.optDouble("axial_length", 160);
				
				JSONObject imuJsonObject = machineJsonObject.optJSONObject("imu");
				IMU imu = new IMU();
				imu.acc1 = imuJsonObject==null ? 0 : imuJsonObject.optInt("acc1", 0);
				imu.acc2 = imuJsonObject==null ? 0 : imuJsonObject.optInt("acc2", 0);
				imu.acc3 = imuJsonObject==null ? 0 : imuJsonObject.optInt("acc3", 0);
				imu.gyro1 = imuJsonObject==null ? 0 : imuJsonObject.optInt("gyro1", 0);
				imu.gyro2 = imuJsonObject==null ? 0 : imuJsonObject.optInt("gyro2", 0);
				imu.gyro3 = imuJsonObject==null ? 0 : imuJsonObject.optInt("gyro3", 0);
				imu.mag1 = imuJsonObject==null ? 0 : imuJsonObject.optInt("mag1", 0);
				imu.mag2 = imuJsonObject==null ? 0 : imuJsonObject.optInt("mag2", 0);
				imu.mag3 = imuJsonObject==null ? 0 : imuJsonObject.optInt("mag3", 0);
				robot.imu = imu;
				
				JSONObject obstacleSensorJsonObject = machineJsonObject.optJSONObject("obstacle_sensor");
				ObstacleSensor obstacleSensor = new ObstacleSensor();
				obstacleSensor.forward = obstacleSensorJsonObject==null ? 0 : obstacleSensorJsonObject.optInt("forward", 0);
				obstacleSensor.backward = obstacleSensorJsonObject==null ? 0 : obstacleSensorJsonObject.optInt("backward", 0);
				obstacleSensor.left = obstacleSensorJsonObject==null ? 0 : obstacleSensorJsonObject.optInt("left", 0);
				obstacleSensor.right = obstacleSensorJsonObject==null ? 0 : obstacleSensorJsonObject.optInt("right", 0);
				robot.obstacle_sensor = obstacleSensor;
				
				JSONArray transmittersJsonArray = machineJsonObject.optJSONArray("transmitters");
				ArrayList<Transmitter> transmitters = new ArrayList<Transmitter>();
				if(transmittersJsonArray!=null && transmittersJsonArray.length()>0){
					int size = transmittersJsonArray.length();
					for(int i=0;i<size;i++){
						JSONObject object = transmittersJsonArray.optJSONObject(i);
						Transmitter transmitter = new Transmitter();
						transmitter.id = object.optInt("id", 0);
						transmitter.offset = object.optInt("offset", 0);
						transmitter.x = object.optInt("x", 0);
						transmitter.y = object.optInt("y", 0);
						transmitter.z = object.optInt("z", 0);
						transmitters.add(transmitter);
					}
				}
				robot.transmitters = transmitters;
				machine = robot;
				break;
			};
			
			seniorSetting.setSetting(machine);
		return seniorSetting;
	}
}
