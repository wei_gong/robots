package cn.rayland.bean;


public class MachineTask<T> {

	protected String taskName;
	protected T content;
	protected TaskCallback taskCallback;
	
	
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public TaskCallback getCallback() {
		return taskCallback;
	}
	public void setCallback(TaskCallback taskCallback) {
		this.taskCallback = taskCallback;
	}
	public T getContent() {
		return content;
	}
	public void setContent(T content) {
		this.content = content;
	}



	public interface TaskCallback {
		void onStart();
		void onError();
		void onCancel();
		void onFinished();
		void onWorking();
	}
	
}
