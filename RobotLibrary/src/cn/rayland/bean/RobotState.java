package cn.rayland.bean;

public class RobotState {
	private float x;
	private float y;
	private float z;
	private float pitch;
	private float roll;
	private float yaw;
	private int vx;
	private int vy;
	private boolean front_obstacle;
	private boolean back_obstacle;
	private boolean left_obstacle;
	private boolean right_obstacle;
	private boolean isError;
	private boolean isWorking;
	private byte[] rfid = new byte[6];
	private int val;
	private int lastLineNun;
	private int soundSensor1;
	private int soundSensor2;
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		this.z = z;
	}
	public float getPitch() {
		return pitch;
	}
	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
	public float getRoll() {
		return roll;
	}
	public void setRoll(float roll) {
		this.roll = roll;
	}
	public float getYaw() {
		return yaw;
	}
	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	public int getVx() {
		return vx;
	}
	public void setVx(int vx) {
		this.vx = vx;
	}
	public int getVy() {
		return vy;
	}
	public void setVy(int vy) {
		this.vy = vy;
	}
	public boolean isFront_obstacle() {
		return front_obstacle;
	}
	public void setFront_obstacle(boolean front_obstacle) {
		this.front_obstacle = front_obstacle;
	}
	public boolean isBack_obstacle() {
		return back_obstacle;
	}
	public void setBack_obstacle(boolean back_obstacle) {
		this.back_obstacle = back_obstacle;
	}
	public boolean isLeft_obstacle() {
		return left_obstacle;
	}
	public void setLeft_obstacle(boolean left_obstacle) {
		this.left_obstacle = left_obstacle;
	}
	public boolean isRight_obstacle() {
		return right_obstacle;
	}
	public void setRight_obstacle(boolean right_obstacle) {
		this.right_obstacle = right_obstacle;
	}
	public boolean isError() {
		return isError;
	}
	public void setError(boolean isError) {
		this.isError = isError;
	}
	public boolean isWorking() {
		return isWorking;
	}
	public void setWorking(boolean isWorking) {
		this.isWorking = isWorking;
	}
	public int getVal() {
		return val;
	}
	public void setVal(int val) {
		this.val = val;
	}
	public int getLastLineNun() {
		return lastLineNun;
	}
	public void setLastLineNun(int lastLineNun) {
		this.lastLineNun = lastLineNun;
	}
	public byte[] getRfid() {
		return rfid;
	}
	public void setRfid(byte[] rfid) {
		this.rfid = rfid;
	}
	public int getSoundSensor1() {
		return soundSensor1;
	}
	public void setSoundSensor1(int soundSensor1) {
		this.soundSensor1 = soundSensor1;
	}
	public int getSoundSensor2() {
		return soundSensor2;
	}
	public void setSoundSensor2(int soundSensor2) {
		this.soundSensor2 = soundSensor2;
	}
	

}
