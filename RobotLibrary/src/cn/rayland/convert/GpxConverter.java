package cn.rayland.convert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import cn.rayland.api.Gpx;
import cn.rayland.api.X3gStatus;

public class GpxConverter implements Converter{
	private String gcodeStr;
	private X3gStatus x3gStatus;
	private byte[] x3gBuffer;
	private volatile boolean stop;
	
	public GpxConverter(String gcodeStr, X3gStatus x3gStatus){
		this.gcodeStr = gcodeStr;
		this.x3gStatus = x3gStatus;
	}

	public void convert() {
		if(gcodeStr == null || gcodeStr.trim().equals("")){
			return;
		}
		if(x3gStatus == null){
			x3gStatus = new X3gStatus(0, 0);
		}
		BufferedReader br = new BufferedReader(new StringReader(gcodeStr));
		String strBuffer = null;
		try {
			while((strBuffer = br.readLine())!=null && !stop){
				if(strBuffer.trim().length()>0){
					x3gBuffer = Gpx.gpxConverPieceGcode(strBuffer, x3gStatus);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cancel() {
		stop = true;
	}

	public byte[] getResult(){
		return x3gBuffer;
	}

	public String getContent() {
		return gcodeStr;
	}

}