package cn.rayland.convert;

public interface Converter {
	void convert();
	void cancel();
}
