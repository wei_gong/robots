package cn.rayland.api;

public class IMU {

	public int acc1;
	public int acc2;
	public int acc3;
	public int gyro1;
	public int gyro2;
	public int gyro3;
	public int mag1;
	public int mag2;
	public int mag3;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IMU other = (IMU) obj;
		if (acc1 != other.acc1)
			return false;
		if (acc2 != other.acc2)
			return false;
		if (acc3 != other.acc3)
			return false;
		if (gyro1 != other.gyro1)
			return false;
		if (gyro2 != other.gyro2)
			return false;
		if (gyro3 != other.gyro3)
			return false;
		if (mag1 != other.mag1)
			return false;
		if (mag2 != other.mag2)
			return false;
		if (mag3 != other.mag3)
			return false;
		return true;
	}
	
	
	
}
