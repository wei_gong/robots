package cn.rayland.api;

import java.util.ArrayList;

/**
 * Created by Lei on 2/4/2016.
 */
public class Robot extends Machine{
		public int machine_type = 4;
		public Motor motor_x;
		public Motor motor_y;
		public Motor motor_a;
		public Motor motor_b;
	    public IMU imu;
	    public double wheel_diameter;
	    public double axial_length;
	    public ObstacleSensor obstacle_sensor;
	    public ArrayList<Transmitter> transmitters;
	    
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Robot other = (Robot) obj;
			if (Double.doubleToLongBits(axial_length) != Double
					.doubleToLongBits(other.axial_length))
				return false;
			if (imu == null) {
				if (other.imu != null)
					return false;
			} else if (!imu.equals(other.imu))
				return false;
			if (motor_a == null) {
				if (other.motor_a != null)
					return false;
			} else if (!motor_a.equals(other.motor_a))
				return false;
			if (motor_b == null) {
				if (other.motor_b != null)
					return false;
			} else if (!motor_b.equals(other.motor_b))
				return false;
			if (motor_x == null) {
				if (other.motor_x != null)
					return false;
			} else if (!motor_x.equals(other.motor_x))
				return false;
			if (motor_y == null) {
				if (other.motor_y != null)
					return false;
			} else if (!motor_y.equals(other.motor_y))
				return false;
			if (obstacle_sensor == null) {
				if (other.obstacle_sensor != null)
					return false;
			} else if (!obstacle_sensor.equals(other.obstacle_sensor))
				return false;
			if (transmitters == null) {
				if (other.transmitters != null)
					return false;
			} else if (!transmitters.equals(other.transmitters))
				return false;
			if (Double.doubleToLongBits(wheel_diameter) != Double
					.doubleToLongBits(other.wheel_diameter))
				return false;
			return true;
		}


	
}
