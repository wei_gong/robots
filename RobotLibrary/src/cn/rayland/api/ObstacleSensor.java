package cn.rayland.api;

public class ObstacleSensor {

	public int forward;
	public int backward;
	public int left;
	public int right;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObstacleSensor other = (ObstacleSensor) obj;
		if (backward != other.backward)
			return false;
		if (forward != other.forward)
			return false;
		if (left != other.left)
			return false;
		if (right != other.right)
			return false;
		return true;
	}

	
}
