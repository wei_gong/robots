package cn.rayland.api;

/**
 * Created by Lei on 2016/1/15.
 */
public class Gpx {
    public native static byte[] getInitX3g(Robot myMachine);
    public native static byte[] gpxConverPieceGcode(String gcode,X3gStatus status);
    public native static int gpxInit(Robot myMachine);
    static{
    	System.loadLibrary("raylandGpx");
    }   
}