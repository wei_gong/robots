package cn.rayland.api;

public class Motor {
	public int pulse_per_round;
	public double reduction_gear_ratio;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Motor other = (Motor) obj;
		if (pulse_per_round != other.pulse_per_round)
			return false;
		if (Double.doubleToLongBits(reduction_gear_ratio) != Double.doubleToLongBits(other.reduction_gear_ratio))
			return false;
		return true;
	}

	
}
