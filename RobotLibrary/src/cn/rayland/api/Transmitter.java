package cn.rayland.api;

public class Transmitter {

	public int id;
	public int x;
	public int y;
	public int z;
	public int offset;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transmitter other = (Transmitter) obj;
		if (id != other.id)
			return false;
		if (offset != other.offset)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		if (z != other.z)
			return false;
		return true;
	}
	
	
}
