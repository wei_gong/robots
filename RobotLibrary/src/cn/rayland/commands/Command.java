package cn.rayland.commands;


public interface Command {
	Object getContent();
	void execute();
	void cancel();
}
