package cn.rayland.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import cn.rayland.api.Gpx;
import cn.rayland.api.X3gStatus;
import cn.rayland.bean.MachineTask.TaskCallback;

public class GcodeCommand implements Command{
	private String content;
	private volatile boolean stop;
	private QueryCommand queryCommand;
	private X3gStatus x3gStatus;
	private X3gCommand executCommand;
	private TaskCallback callback;

	public GcodeCommand(String gcodeStr, QueryCommand command, TaskCallback callback){
		this.content = gcodeStr;
		this.queryCommand = command;
		this.stop = false;
		this.x3gStatus = new X3gStatus(0, 0);
		this.callback = callback;
	}
	
	public void execute() {
		if(content == null || content.trim().equals("")){
			return;
		}
		BufferedReader br = new BufferedReader(new StringReader(content));
		String strBuffer = null;
		if(callback != null){
			callback.onStart();
		}
		try {
			while(!stop){
				queryCommand.execute();
				if(callback != null){
					callback.onWorking();
				}
				if (queryCommand.getContent().isError()) {
					if(callback != null){
						callback.onError();
					}
					stop = true;
				} else if((strBuffer = br.readLine()) != null) {
					if (strBuffer != null && strBuffer.trim().length() > 0) {
						byte[] x3g = Gpx.gpxConverPieceGcode(strBuffer,
								x3gStatus);
						TaskCallback x3gCallback = new TaskCallback() {

							public void onStart() {
								// TODO Auto-generated method stub
								
							}

							public void onError() {
								// TODO Auto-generated method stub
								
							}

							public void onCancel() {
								// TODO Auto-generated method stub
								
							}

							public void onFinished() {
								// TODO Auto-generated method stub
								
							}

							public void onWorking() {
								if(callback != null){
									callback.onWorking();
								}
							}
						};
							executCommand = new X3gCommand(x3g, queryCommand, x3gCallback);
							if(!stop){
								executCommand.execute();	
							}
					}
				} else {
					if(callback != null){
						if(!queryCommand.getContent().isWorking()){
							callback.onFinished();
							stop = true;
						}
					}else{
						stop = true;
					}
			}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cancel() {
		if(callback != null){
			callback.onCancel();
		}
		stop = true;
		if(executCommand != null){
			executCommand.cancel();
		}
	}
	
	public String getContent() {
		return content;
	}

}
