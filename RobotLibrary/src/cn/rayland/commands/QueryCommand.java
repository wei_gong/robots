package cn.rayland.commands;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android_serialport_api.SerialPort;
import cn.rayland.bean.RobotState;
import cn.rayland.library.stm32.IIC;
import cn.rayland.utils.ZMQManager;

public class QueryCommand implements Command{
	private volatile RobotState state;
	private IIC stm32;
	private byte[] datas = new byte[26];
	private SerialPort serialPort;
	private InputStream inputStream;
	private OutputStream outputStream;
	private byte[] reqRfid = {0X01, 0X08, (byte) 0XA1, 0X20, 0X00, 0X01, 0X00, 0X76};
	private ExecutorService rfidService;
	
	public QueryCommand(final RobotState state){
		this.state = state;
		this.stm32 = IIC.getInstance();
		rfidService = Executors.newSingleThreadExecutor();
	}
	
	public void execute() {
		stm32.readData(datas.length, datas);
		if (datas != null) {
			state.setVal((datas[0]&0XFF) | ((datas[1]&0XFF)<<8));
			state.setX((datas[2]&0XFF) | ((datas[3]&0XFF)<<8));
			state.setY((datas[4]&0XFF) | ((datas[5]&0XFF)<<8));
			state.setZ((datas[6]&0XFF) | ((datas[7]&0XFF)<<8));
			state.setYaw((datas[8]&0XFF) | ((datas[9]&0XFF)<<8));
			state.setRoll((datas[10]&0XFF) | ((datas[11]&0XFF)<<8));
			state.setPitch((datas[12]&0XFF) | ((datas[13]&0XFF)<<8));
			state.setVx((datas[14]&0XFF) | ((datas[15])<<8));
			state.setVy((datas[16]&0XFF) | ((datas[17])<<8));
			state.setFront_obstacle((datas[18] & 1)!=0);
			state.setBack_obstacle((datas[18] & (1 << 1))!=0);
			state.setLeft_obstacle((datas[18] & (1 << 2))!=0);
			state.setRight_obstacle((datas[18] & (1 << 3))!=0);
			state.setWorking((datas[19] & 1)!=0);
			state.setError((datas[19] & (1 << 7))!=0);
			state.setLastLineNun((datas[20]&0XFF) | ((datas[21]&0XFF)<<8));
			state.setSoundSensor1((datas[22]&0XFF) | ((datas[23]&0XFF)<<8));
			state.setSoundSensor2((datas[24]&0XFF) | ((datas[25]&0XFF)<<8));
			byte[] bytes = new byte[]{(byte) (datas[14]&0XFF), datas[15], (byte) (datas[16]&0XFF), datas[17]};
			ZMQManager.sendFeedback(bytes);
			rfidService.execute(rfidCheckRunnable);
		}	
	}
	
	private Runnable rfidCheckRunnable = new Runnable() {
		
		@Override
		public void run() {
				try {
					if (serialPort == null || inputStream == null
							|| outputStream == null) {
						serialPort = new SerialPort(new File("/dev/ttyUSB0"),
								9600, 0);
						inputStream = serialPort.getInputStream();
						outputStream = serialPort.getOutputStream();
					}
					outputStream.write(reqRfid);
					inputStream.skip(4);
					boolean success = inputStream.read() == 0;
					if (success) {
						byte[] content = new byte[7];
						inputStream.read(content);
						for (int i = 0; i < 6; i++) {
							state.getRfid()[i] = content[i];
						}
					} else {
						inputStream.read(new byte[3]);
						for (int i = 0; i < 6; i++) {
							state.getRfid()[i] = 0;
						}
					}
				} catch (SecurityException | IOException e) {
					return;
				}
		}
	};

	public void cancel() {
		
	}

	public RobotState getContent() {
		return state;
	}
	
}
