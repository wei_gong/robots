package cn.rayland.commands;

import java.io.ByteArrayInputStream;
import android.util.Log;
import cn.rayland.bean.MachineTask.TaskCallback;
import cn.rayland.library.stm32.IIC;

public class X3gCommand implements Command{
	private IIC stm32;
	private byte[] content;
	private volatile boolean stop;
	private QueryCommand queryCommand;
	public static final int CHUNK = 10;
	private TaskCallback callback;
	private boolean stopWhenError = true;

	public X3gCommand(byte[] data, QueryCommand command, TaskCallback callback){
		this.content = data;
		this.queryCommand = command;
		this.stm32 = IIC.getInstance();
		this.callback = callback;
	}

	public void execute() {
		ByteArrayInputStream bis = new ByteArrayInputStream(content);
		byte[] tmpBytes = new byte[CHUNK + 2];
		int length = -1;
		if(callback != null){
			callback.onStart();
		}
		while (!stop) {
			queryCommand.execute();
			if(callback != null){
				callback.onWorking();
			}
			if (queryCommand.getContent().isError()) {
				if(callback != null){
					callback.onError();
				}
				if(stopWhenError){
					stop = true;
				}
			} else if (queryCommand.getContent().getVal() > CHUNK) {
				if ((length = bis.read(tmpBytes, 2, CHUNK)) != -1) {
					tmpBytes[0] = 0;
					tmpBytes[1] = 0;
					if(!stop) {
						stm32.sendData(tmpBytes, 0, length+2);
//						X3gParser.printHexString(tmpBytes);
					}
				} else {
					if(callback != null){
						if(!queryCommand.getContent().isWorking()){
							callback.onFinished();
							stop = true;
						}
					}else{
							stop = true;
						}
				}
			}
		}
	}

	public void cancel() {
		if(callback != null){
			callback.onCancel();
		}
		stop = true;
		Log.d("DEBUG", "cancel send");
	}

	public byte[] getContent() {
		return content;
	}
	
	public void setStopWhenError(boolean stopWhenError){
		this.stopWhenError = stopWhenError;
	}
}