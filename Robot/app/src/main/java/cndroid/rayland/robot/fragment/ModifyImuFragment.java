package cndroid.rayland.robot.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import cn.rayland.api.IMU;
import cn.rayland.robot.R;
import cndroid.rayland.robot.utils.ToastUtils;

/**
 * Created by gw on 2016/10/26.
 */

public class ModifyImuFragment extends BaseFragment {
    @InjectView(R.id.tv_imu_acc1_value)
    TextView imuAcc1;
    @InjectView(R.id.tv_imu_acc2_value)
    TextView imuAcc2;
    @InjectView(R.id.tv_imu_acc3_value)
    TextView imuAcc3;
    @InjectView(R.id.tv_imu_gyro1_value)
    TextView imuGyro1;
    @InjectView(R.id.tv_imu_gyro2_value)
    TextView imuGyro2;
    @InjectView(R.id.tv_imu_gyro3_value)
    TextView imuGyro3;
    @InjectView(R.id.tv_imu_mag1_value)
    TextView imuMag1;
    @InjectView(R.id.tv_imu_mag2_value)
    TextView imuMag2;
    @InjectView(R.id.tv_imu_mag3_value)
    TextView imuMag3;
    private IMU imu;

    public static ModifyImuFragment getInstance(IMU imu){
        ModifyImuFragment fragment = new ModifyImuFragment();
        fragment.imu = imu;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_modify_imu, container, false);
        ButterKnife.inject(this,view);
        fillContent();
        return view;
    }

    @OnClick({R.id.rl_imu_acc1_layout, R.id.rl_imu_acc2_layout, R.id.rl_imu_acc3_layout, R.id.rl_imu_gyro1_layout, R.id.rl_imu_gyro2_layout, R.id.rl_imu_gyro3_layout, R.id.rl_imu_mag1_layout, R.id.rl_imu_mag2_layout, R.id.rl_imu_mag3_layout})
    void onClick(View view){
        if(imu == null){
            return;
        }
        final EditText editText;
        switch (view.getId()){
            case R.id.rl_imu_acc1_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_acc1_describe);
                showModifyDialog(R.string.imu_acc1, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuAcc1.setText(editText.getText().toString().trim());
                            imu.acc1 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
            case R.id.rl_imu_acc2_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_acc2_describe);
                showModifyDialog(R.string.imu_acc2, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuAcc2.setText(editText.getText().toString().trim());
                            imu.acc2 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
            case R.id.rl_imu_acc3_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_acc3_describe);
                showModifyDialog(R.string.imu_acc3, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuAcc3.setText(editText.getText().toString().trim());
                            imu.acc3 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
            case R.id.rl_imu_gyro1_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_gyro1_describe);
                showModifyDialog(R.string.imu_gyro1, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuGyro1.setText(editText.getText().toString().trim());
                            imu.gyro1 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
            case R.id.rl_imu_gyro2_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_gyro2_describe);
                showModifyDialog(R.string.imu_gyro2, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuGyro2.setText(editText.getText().toString().trim());
                            imu.gyro2 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
            case R.id.rl_imu_gyro3_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_gyro3_describe);
                showModifyDialog(R.string.imu_gyro3, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuGyro3.setText(editText.getText().toString().trim());
                            imu.gyro3 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
            case R.id.rl_imu_mag1_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_mag1_describe);
                showModifyDialog(R.string.imu_mag1, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuMag1.setText(editText.getText().toString().trim());
                            imu.mag1 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
            case R.id.rl_imu_mag2_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_mag2_describe);
                showModifyDialog(R.string.imu_mag2, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuMag1.setText(editText.getText().toString().trim());
                            imu.mag2 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
            case R.id.rl_imu_mag3_layout:
                editText = new EditText(context);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_SIGNED);
                editText.setHint(R.string.imu_mag3_describe);
                showModifyDialog(R.string.imu_mag3, editText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            imuMag3.setText(editText.getText().toString().trim());
                            imu.mag3 = Integer.parseInt(editText.getText().toString().trim());
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
                break;
        }
    }

    private void fillContent() {
        if(imu != null){
            imuAcc1.setText(String.valueOf(imu.acc1));
            imuAcc2.setText(String.valueOf(imu.acc2));
            imuAcc3.setText(String.valueOf(imu.acc3));
            imuGyro1.setText(String.valueOf(imu.gyro1));
            imuGyro2.setText(String.valueOf(imu.gyro2));
            imuGyro3.setText(String.valueOf(imu.gyro3));
            imuMag1.setText(String.valueOf(imu.mag1));
            imuMag2.setText(String.valueOf(imu.mag2));
            imuMag3.setText(String.valueOf(imu.mag3));
        }
    }

    @Override
    public void onDestroyView() {
        ButterKnife.reset(this);
        super.onDestroyView();
    }

    private void showModifyDialog(int titleId, View view, DialogInterface.OnClickListener confirmListener){
        new AlertDialog.Builder(context).setTitle(titleId).setView(view).setPositiveButton(R.string.confirm, confirmListener).setNegativeButton(R.string.cancel, null).show();
    }
}
