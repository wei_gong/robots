package cndroid.rayland.robot.bean;

/**
 * Created by gw on 2016/3/9.
 */
public class WBSocketMsg {
    private String cmd;
    private String data;
    private String name;
    private String imei;
    private byte[] gcode;

    public byte[] getGcode() {
        return gcode;
    }

    public void setGcode(byte[] gcode) {
        this.gcode = gcode;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
