package cndroid.rayland.robot.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;


import cn.rayland.bean.MachineTask;
import cndroid.rayland.robot.App;
import cndroid.rayland.robot.bean.WBSocketMsg;
import cndroid.rayland.robot.utils.ToastUtils;
import cndroid.rayland.robot.utils.WebSocketUtil;
import de.greenrobot.event.EventBus;

/**
 * Created by gw on 2016/3/9.
 */
public class SocketMsgService extends Service {
    private final static String TAG = SocketMsgService.class.getSimpleName();
    private MachineTask<String> task = new MachineTask<>();
    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler(Looper.getMainLooper());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "service startCommand");
            WebSocketUtil.open();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        WebSocketUtil.close();
        EventBus.getDefault().unregister(this);
        Log.e(TAG, "service destory");
    }

    /**
     * 接收远程信息
     *
     * @param event
     */
    public void onEventMainThread(final WBSocketMsg event) {
        final Context context = getApplicationContext();
        final String imei = event.getImei();
        final String name = event.getName();
        String data = event.getData();
        if (data.equals(WebSocketUtil.START_GET_LOCATION)) {
            WebSocketUtil.remoteImei = imei;
        }else if(data.equals(WebSocketUtil.STOP_GET_LOCATION)){
            WebSocketUtil.remoteImei = null;
        }
        else if (data.startsWith("G6 ")){
            task.setCallback(null);
            task.setContent(event.getData());
            App.manager.sendTask(task, true);
            WebSocketUtil.remoteImei = imei;
        }else if (data.equals(WebSocketUtil.STOP_EXECUTE)){
            App.manager.stop(true);
        }else if (data.startsWith("G9 ")){
            String gcodeStr = data.substring(0,data.lastIndexOf(" D"));
            String distance_threshold = data.substring(data.lastIndexOf(" D")+2);
            task.setContent(gcodeStr);
            task.setCallback(new MachineTask.TaskCallback() {
                @Override
                public void onStart() {
                }

                @Override
                public void onWorking() {
                }

                @Override
                public void onError() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtils.showShort(SocketMsgService.this, "Error!!!");
                        }
                    });
                }

                @Override
                public void onCancel() {
                }

                @Override
                public void onFinished() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtils.showShort(SocketMsgService.this, "Already Arrived!!!");
                        }
                    });
                }
            });
            App.manager.sendPilotTask(task, Float.parseFloat(distance_threshold));
            WebSocketUtil.remoteImei = imei;
        }
    }

}
