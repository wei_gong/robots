package cndroid.rayland.robot.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import cn.rayland.robot.R;


/**
 * Created by wuyaxian on 2016/3/29.
 */
public class LineChartView extends View{
    private Paint paintText, paintPoint, paint;
    private int mW, mH;
    private float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
    private float radius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics());
    private float tab = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
    private static final int unit = 20;
    private volatile float x, y, yaw;
    private float[] target = new float[2];

    public LineChartView(Context context) {
        this(context, null);
    }

    public LineChartView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LineChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.mW = getMeasuredWidth();
        this.mH = getMeasuredHeight();
    }

    public void init() {
        paintText = new Paint();
        paintText.setColor(Color.WHITE);
        paintText.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 14, getResources().getDisplayMetrics()));
        paintText.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics()));
        paintText.setAntiAlias(true);
        paintText.setTextAlign(Paint.Align.CENTER);
        paintPoint = new Paint();
        paintPoint.setColor(Color.RED);
        paintPoint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics()));
        paintPoint.setAntiAlias(true);
        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics()));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        target[0] = padding;
        target[1] = padding;
    }

    private void drawXY(Canvas canvas) {
        //X轴
        canvas.drawLine(padding, padding, mW, padding, paintText);
        canvas.drawLine(mW, padding, mW - tab, padding - tab, paintText);
        canvas.drawLine(mW, padding, mW - tab, padding + tab, paintText);
        //Y轴
        canvas.drawLine(padding, padding, padding, mH, paintText);
        canvas.drawLine(padding, mH, padding - tab, mH - tab, paintText);
        canvas.drawLine(padding, mH, padding + tab, mH - tab, paintText);
        canvas.drawText("0", padding - tab, padding, paintText);
        canvas.drawText("X", mW - tab*4, padding + tab * 4, paintText);
        canvas.drawText("Y", padding + tab*4, mH - tab * 4, paintText);

        /**
         * 画坐标尺
         */
        for (int i = 1; i <= mW/padding; i++) {
            canvas.drawLine(padding * (i + 1), padding, padding * (i + 1), padding + tab, paintText);
            if( i%2 == 0){
                canvas.drawText(unit * i + "", padding * (i + 1), padding/2, paintText);
            }
        }
        for(int i = 1; i <= mH/padding; i++){
            canvas.drawLine(padding, padding * (i + 1), padding + tab, padding * (i + 1), paintText);
            if( i%2 == 0){
                canvas.drawText(unit * i + "", padding/2, padding * (i + 1), paintText);
            }
        }


//        /**
//         *画房间固定点
//         */
//        canvas.drawCircle(padding, padding, radius, paintPoint);
//        canvas.drawCircle(padding, padding + ((padding * 9) - 8), radius, paintPoint);
//        canvas.drawCircle(padding + ((padding * 4) - 2), padding + ((padding * 7) - 2), radius, paintPoint);
//        canvas.drawCircle(padding + ((padding * 4) - 3), padding + (padding * 3), radius, paintPoint);
//        canvas.drawCircle(padding + ((padding * 8) - 6), padding + ((padding * 9) - 8), radius, paintPoint);
//        canvas.drawCircle(padding + ((padding * 7) + 4), padding, radius, paintPoint);
//        canvas.drawCircle(padding + (padding * 11), padding + ((padding * 9) + 3), radius, paintPoint);
//        canvas.drawCircle(padding + ((padding * 10) - 4), padding + padding + 7, radius, paintPoint);
//        canvas.drawCircle(padding + (padding * 14), padding + ((padding * 5) + 5), radius, paintPoint);

        /**
         * 画当前坐标值
         */
        String text = "当前:（"+Math.round(x)+"，"+Math.round(y)+"）";
        canvas.drawText(text,mW/2,mH*7/8, paintText);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        doDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                target[0] = event.getX();
                target[1] = event.getY();
                invalidate();
                break;
        }
        return super.onTouchEvent(event);
    }

    /**
     * 实时更新robot运动的轨迹坐标
     */
    public void doDraw(Canvas canvas) {
        drawXY(canvas);
        drawTarget(canvas);
        drawLocation(canvas);
    }

    private void drawTarget(Canvas canvas) {
        canvas.drawCircle(target[0], target[1], 2*radius, paint);
        String text = "("+Math.round(getMovePoint(target[0]))+","+Math.round(getMovePoint(target[1]))+")";
        Paint.FontMetrics fm = paintText.getFontMetrics();
        float textHeight = fm.bottom - fm.top;
        canvas.drawText(text, target[0], target[1]+(2*radius)+(textHeight/2)+4, paintText);
    }

    private void drawLocation(Canvas canvas) {
        float px = getPoint(x);
        float py = getPoint(y);
        Matrix matrix = new Matrix();
        matrix.setRotate(135 + yaw);
        Bitmap locationBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.location);
        Bitmap bitmap1 = Bitmap.createBitmap(locationBitmap, 0, 0, locationBitmap.getWidth(), locationBitmap.getHeight(), matrix, false);
        canvas.drawBitmap(bitmap1,px - bitmap1.getWidth() / 2,py - bitmap1.getHeight()/2,null);
        bitmap1.recycle();
        locationBitmap.recycle();
    }

    /**
     * 实际坐标与图上坐标比例换算
     */
    float getPoint(float point)
    {
        return padding + (padding/unit)* point;
    }

    float getMovePoint(float dp) {
        return (dp -padding)/(padding/unit);
    }

    /**
     * 获取当前x/y/yaw
     */
    public void setMove(float x, float y, float yaw) {
        this.x = x;
        this.y = y;
        this.yaw = yaw;
        postInvalidate();
    }

    public float[] getTarget(){
        float[] realityTarget = new float[2];
        realityTarget[0] = getMovePoint(target[0]);
        realityTarget[1] = getMovePoint(target[1]);
        return realityTarget;
    }

}
