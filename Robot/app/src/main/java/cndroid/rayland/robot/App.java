package cndroid.rayland.robot;

import android.app.Application;
import android.content.Context;
import cn.rayland.utils.RobotManager;
import cndroid.rayland.robot.utils.PathUtils;


/**
 * Created by jinbangzhu on 6/23/15.
 */
public class App extends Application {
    private static App app = null;
    public static RobotManager manager;
    public static final String CONFIG_DIR = PathUtils.getMachineFolder();
    public static final String CONFIG_FILE_NAME = "machine.txt";
    public static final String CONFIG_PATH = CONFIG_DIR+"/"+CONFIG_FILE_NAME;
    @Override
    public void onCreate() {
        super.onCreate();
        if(app == null){
            app = this;
        }
        manager = RobotManager.getInstance(this);
//        manager.setCustomMachineConfig(CONFIG_PATH);
    }

    public static App getInstance() {
        return app;
    }

    public static Context getContext(){
        return app.getApplicationContext();
    }
}
