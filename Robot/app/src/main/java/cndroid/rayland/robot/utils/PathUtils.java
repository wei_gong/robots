package cndroid.rayland.robot.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by jin on 2014/6/14.
 */
public class PathUtils {
    public static String getMainFolder() {
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            return null;
        }
        File extStore = Environment.getExternalStorageDirectory();
        String path = extStore.getAbsolutePath() + "/Robot/";
        final File file = new File(path);
        if (!file.exists())
            file.mkdir();
        return path;
    }
    public static String getVideoFolder(){
        String path = getMainFolder() + "video/";
        final File file = new File(path);
        if (!file.exists())
            file.mkdir();
        return path;
    }

    public static String getRemoteFolder() {
        String path = getMainFolder() + "remoteFile";
        final File file = new File(path);
        if (!file.exists())
            file.mkdir();
        return path;
    }

    public static String getMachineFolder(){
        String path = getMainFolder() + "machine";
        final File file = new File(path);
        if (!file.exists())
            file.mkdir();
        return path;
    }

    public static String getCuraFolder(){
        String path = getMainFolder() + "cura";
        final File file = new File(path);
        if (!file.exists())
            file.mkdir();
        return path;
    }

    public static String getGpxFolder(){
        String path = getMainFolder() + "gpx";
        final File file = new File(path);
        if (!file.exists())
            file.mkdir();
        return path;
    }

}
