package cndroid.rayland.robot.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import cn.rayland.bean.RobotState;
import cn.rayland.robot.R;
import cndroid.rayland.robot.App;

/**
 * Created by gwlab on 2016/5/14.
 */
public class MonitorFragment extends BaseFragment {
    @InjectView(R.id.tv_x)
    TextView tv_x;
    @InjectView(R.id.tv_y)
    TextView tv_y;
    @InjectView(R.id.tv_z)
    TextView tv_z;
    @InjectView(R.id.tv_vx)
    TextView tv_vx;
    @InjectView(R.id.tv_vy)
    TextView tv_vy;
    @InjectView(R.id.tv_roll)
    TextView tv_roll;
    @InjectView(R.id.tv_pitch)
    TextView tv_pitch;
    @InjectView(R.id.tv_yaw)
    TextView tv_yaw;
    @InjectView(R.id.tv_front)
    TextView tv_front;
    @InjectView(R.id.tv_back)
    TextView tv_back;
    @InjectView(R.id.tv_left)
    TextView tv_left;
    @InjectView(R.id.tv_right)
    TextView tv_right;
    @InjectView(R.id.tv_val)
    TextView tv_val;
    @InjectView(R.id.tv_lastlinenum)
    TextView tv_lastlinenum;
    @InjectView(R.id.tv_rfid)
    TextView tv_rfid;
    @InjectView(R.id.tv_sound1)
    TextView tv_sound1;
    @InjectView(R.id.tv_sound2)
    TextView tv_sound2;
    @InjectView(R.id.tv_working)
    TextView tv_working;
    @InjectView(R.id.tv_error)
    TextView tv_error;
    private Handler handler = new Handler();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_monitor, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                RobotState state = App.manager.getRobotState();
                tv_x.setText(String.valueOf(state.getX()));
                tv_y.setText(String.valueOf(state.getY()));
                tv_z.setText(String.valueOf(state.getZ()));
                tv_vx.setText(String.valueOf(state.getVx()));
                tv_vy.setText(String.valueOf(state.getVy()));
                tv_working.setText(String.valueOf(state.isWorking()));
                tv_error.setText(String.valueOf(state.isError()));
                tv_roll.setText(String.valueOf(state.getRoll()));
                tv_pitch.setText(String.valueOf(state.getPitch()));
                tv_yaw.setText(String.valueOf(state.getYaw()));
                tv_front.setText(String.valueOf(state.isFront_obstacle()));
                tv_back.setText(String.valueOf(state.isBack_obstacle()));
                tv_left.setText(String.valueOf(state.isLeft_obstacle()));
                tv_right.setText(String.valueOf(state.isRight_obstacle()));
                tv_val.setText(String.valueOf(state.getVal()));
                tv_lastlinenum.setText(String.valueOf(state.getLastLineNun()));
                tv_rfid.setText(byte2HexStr(state.getRfid()));
                tv_sound1.setText(String.valueOf(state.getSoundSensor1()));
                tv_sound2.setText(String.valueOf(state.getSoundSensor2()));
                handler.postDelayed(this, 100);
            }
        };
        handler.postDelayed(runnable, 100);
    }

    private static String byte2HexStr(byte[] b) {
        if (b == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder("");
        for (int n = 0; n < b.length; n++) {
            String stmp = Integer.toHexString(b[n] & 0xFF);
            sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    private static byte[] hexStr2Bytes(String src) {
        if (src == null) {
            return null;
        }
        String[] strs = src.split(" ");
        int l = strs.length;
        byte[] ret = new byte[l];
        for (int i = 0; i < l; i++) {
            ret[i] = Byte.decode("0x" + strs[i]);
        }
        return ret;
    }
}
