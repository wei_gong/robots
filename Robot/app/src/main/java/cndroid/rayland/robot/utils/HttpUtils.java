package cndroid.rayland.robot.utils;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.File;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpClient;

/**
 * Created by jinbangzhu on 7/10/15.
 */
public class HttpUtils {
    private static final String TAG = HttpClient.class.getSimpleName();
    private static HttpUtils ourInstance = new HttpUtils();
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static OkHttpClient client = new OkHttpClient();
    private static AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

    public static HttpUtils getInstance() {
        return ourInstance;
    }

    static {
        asyncHttpClient.setTimeout(60 * 1000 * 10);
    }

    private HttpUtils() {
    }


    public void post(String url, Object cls, Callback callback) {
        post(url, com.alibaba.fastjson.JSON.toJSONString(cls), callback);
    }

    public void post(String url, String json, Callback callback) {
//        String token = EasySharePreference.getToken(App.getInstance());
        RequestBody body = RequestBody.create(JSON, json);
        final Request request = new Request.Builder()
                .url(url)
                .post(body)
//                .addHeader("authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjQ1fQ.szj0HMOIVK8DwQ-8_lwiuHRV2TkmeXEPWcKC2s99dZQ")
                .addHeader("authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjc0fQ.hoUTQRULE2Q5jPs6Iyl--F47xS1hqHzjWUJWtp-DLW4")
//                .addHeader("authorization", "jian")
                .build();
        client.newCall(request).enqueue(callback);
    }

    public void get(String url, Callback callback) {
//        String token = EasySharePreference.getToken(App.getInstance());
        final Request request = new Request.Builder()
                .url(url)
//                .addHeader("authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjQ1fQ.szj0HMOIVK8DwQ-8_lwiuHRV2TkmeXEPWcKC2s99dZQ")
                .addHeader("authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjc0fQ.hoUTQRULE2Q5jPs6Iyl--F47xS1hqHzjWUJWtp-DLW4")
//                .addHeader("authorization", "jian")
                .build();

        client.newCall(request).enqueue(callback);
    }



    public void postFile(final Context context, String url, RequestParams requestParams, TextHttpResponseHandler httpResponseHandler){

        asyncHttpClient.post(context, url, requestParams, httpResponseHandler);
    }


    public void downloadFile(final Context context, String url, FileAsyncHttpResponseHandler handler) {
        Log.d(TAG, "");
        asyncHttpClient.get(context, url, handler);
    }
    public void getUpdate(String url, Callback callback) {
        final Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(callback);
    }

    public static void downloadVideo(final Context context, String url, final String saveFile) {
        asyncHttpClient.get(context, url, new FileAsyncHttpResponseHandler(new File(saveFile)) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Log.d("", "onFailure = downloadFile " + throwable.getMessage());
                if (file.exists())
                    file.delete();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                Log.d("", "onSuccess = downloadFile");

            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }

}
