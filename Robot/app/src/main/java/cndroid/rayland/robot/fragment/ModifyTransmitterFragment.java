package cndroid.rayland.robot.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.List;
import butterknife.ButterKnife;
import butterknife.InjectView;
import cn.rayland.api.Transmitter;
import cn.rayland.robot.R;
import cndroid.rayland.robot.adapter.TransmitterAdapter;
import cndroid.rayland.robot.utils.ToastUtils;

/**
 * Created by gw on 2016/10/26.
 */

public class ModifyTransmitterFragment extends BaseFragment {
    @InjectView(R.id.rv_transmitter_list)
    RecyclerView transmitterList;
    private TransmitterAdapter adapter;
    private List<Transmitter> transmitters;
    private View dialogView;
    private EditText etId;
    private EditText etOffset;
    private EditText etCoordinateX;
    private EditText etCoordinateY;
    private EditText etCoordinateZ;
    private AlertDialog dialog;

    public static ModifyTransmitterFragment getInstance(List<Transmitter> transmitters){
        ModifyTransmitterFragment fragment = new ModifyTransmitterFragment();
        fragment.transmitters = transmitters;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_modify_transmitters, container, false);
        ButterKnife.inject(this, view);
        initDialogView();
        adapter = new TransmitterAdapter(context, transmitters);
        adapter.setOnClickItemListener(new TransmitterAdapter.OnClickItemListener() {
            @Override
            public void onClickItem(View view, final int position) {
                final Transmitter transmitter = transmitters.get(position);
                showModifyTransmitterDialog(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try{
                            transmitter.id = Integer.parseInt(etId.getText().toString().trim());
                            transmitter.offset = Integer.parseInt(etOffset.getText().toString().trim());
                            transmitter.x = Integer.parseInt(etCoordinateX.getText().toString().trim());
                            transmitter.y = Integer.parseInt(etCoordinateY.getText().toString().trim());
                            transmitter.z = Integer.parseInt(etCoordinateZ.getText().toString().trim());
                            adapter.notifyItemChanged(position);
                        }catch (Exception e){
                            e.printStackTrace();
                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
                        }
                    }
                });
            }
        });
        transmitterList.setAdapter(adapter);
        transmitterList.setLayoutManager(new LinearLayoutManager(context));
        return view;
    }

    private void initDialogView() {
        dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_modify_transmitter, null);
        etId = (EditText) dialogView.findViewById(R.id.et_id);
        etOffset = (EditText) dialogView.findViewById(R.id.et_offset);
        etCoordinateX = (EditText) dialogView.findViewById(R.id.et_coordinate_x);
        etCoordinateY = (EditText) dialogView.findViewById(R.id.et_coordinate_y);
        etCoordinateZ = (EditText) dialogView.findViewById(R.id.et_coordinate_z);
        dialog =  new AlertDialog.Builder(context).setView(dialogView).create();

    }

    @Override
    public void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }

    private void showModifyTransmitterDialog(DialogInterface.OnClickListener listener){
        dialogView.setSaveEnabled(false);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.confirm), listener);
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }
}
