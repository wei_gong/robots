//package cndroid.rayland.robot.fragment;
//
//import android.app.Activity;
//import android.content.DialogInterface;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.widget.Toolbar;
//import android.text.InputType;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.EditText;
//import android.widget.TextView;
//
//import java.util.List;
//
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;
//import cn.rayland.api.IMU;
//import cn.rayland.api.Machine;
//import cn.rayland.api.Robot;
//import cn.rayland.api.Transmitter;
//import cn.rayland.robot.R;
//import cndroid.rayland.robot.utils.ToastUtils;
//
///**
// * Created by gw on 2016/10/26.
// */
//
//public class ModifyMachineFragment extends BaseFragment {
//    @InjectView(R.id.tv_left_wheel_diameter_value)
//    TextView leftWheelDiameter;
//    @InjectView(R.id.tv_right_wheel_diameter_value)
//    TextView rightWheelDiameter;
//    @InjectView(R.id.tv_axial_length_value)
//    TextView axialLength;
//    @InjectView(R.id.tv_turn_direction_value)
//    TextView turnDirection;
//    @InjectView(R.id.tv_obstacle_sensor_value)
//    TextView obstacleSensor;
//    Toolbar toolbar;
//    private Robot machine;
//
//    public static ModifyMachineFragment getInstance(Robot machine){
//        ModifyMachineFragment fragment = new ModifyMachineFragment();
//        fragment.machine = machine;
//        return fragment;
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_modify_machine, container, false);
//        ButterKnife.inject(this,view);
//        toolbar = (Toolbar) ((Activity)context).findViewById(R.id.toolbar);
//        toolbar.getMenu().clear();
//        toolbar.inflateMenu(R.menu.main);
//        toolbar.setNavigationIcon(null);
//        toolbar.setTitle(R.string.machine_config);
//        fillContent();
//        return view;
//    }
//
//    @OnClick({R.id.rl_left_wheel_diameter_layout, R.id.rl_right_wheel_diameter_layout, R.id.rl_axial_length_layout, R.id.rl_turn_direction_layout, R.id.rl_obstacle_sensor_layout, R.id.rl_imu_layout, R.id.rl_transmitters_layout})
//    void onClick(View view){
//        if(machine == null){
//            return;
//        }
//        final EditText editText;
//        final String[] items;
//        switch (view.getId()){
//            case R.id.rl_left_wheel_diameter_layout:
//                editText = new EditText(context);
//                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
//                editText.setHint(R.string.left_wheel_diameter_describe);
//                showModifyDialog(R.string.left_wheel_diameter, editText, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        try{
//                            leftWheelDiameter.setText(editText.getText().toString().trim());
//                            machine.axis_x.length = Double.parseDouble(editText.getText().toString().trim());
//                        }catch (Exception e){
//                            e.printStackTrace();
//                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
//                        }
//                    }
//                });
//                break;
//            case R.id.rl_right_wheel_diameter_layout:
//                editText = new EditText(context);
//                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
//                editText.setHint(R.string.right_wheel_diameter_describe);
//                showModifyDialog(R.string.right_wheel_diameter, editText, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        try{
//                            rightWheelDiameter.setText(editText.getText().toString().trim());
//                            machine.axis_y.length = Double.parseDouble(editText.getText().toString().trim());
//                        }catch (Exception e){
//                            e.printStackTrace();
//                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
//                        }
//                    }
//                });
//                break;
//            case R.id.rl_axial_length_layout:
//                editText = new EditText(context);
//                editText.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
//                editText.setHint(R.string.axial_length_describe);
//                showModifyDialog(R.string.axial_length, editText, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        try{
//                            axialLength.setText(editText.getText().toString().trim());
//                            machine.axis_z.length = Double.parseDouble(editText.getText().toString().trim());
//                        }catch (Exception e){
//                            e.printStackTrace();
//                            ToastUtils.showShort(context, getString(R.string.format_error_to_save));
//                        }
//                    }
//                });
//                break;
//            case R.id.rl_turn_direction_layout:
//                items = getResources().getStringArray(R.array.turn_direction_type);
//                showSingleChoiceDialog(R.string.turn_direction, items, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        turnDirection.setText(items[which]);
//                        machine.type = which;
//                    }
//                });
//                break;
//            case R.id.rl_obstacle_sensor_layout:
//                items = getResources().getStringArray(R.array.obstacle_sensor_type);
//                final boolean[] checked = new boolean[items.length];
//                checked[0] = machine.obstacle_sensor.forward == 1;
//                checked[1] = machine.obstacle_sensor.backward == 1;
//                checked[2] = machine.obstacle_sensor.left == 1;
//                checked[3] = machine.obstacle_sensor.right == 1;
//                showMultiChoiceDialog(R.string.obstacle_sensor, items, checked, new DialogInterface.OnMultiChoiceClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//                        checked[which] = isChecked;
//                        StringBuilder values = new StringBuilder();
//                        for(int i=0;i<items.length;i++){
//                            if(checked[i]) values.append(items[i]).append(" ");
//                        }
//                        obstacleSensor.setText(values.toString());
//                        machine.obstacle_sensor.forward = checked[0]?1:0;
//                        machine.obstacle_sensor.backward = checked[1]?1:0;
//                        machine.obstacle_sensor.left = checked[2]?1:0;
//                        machine.obstacle_sensor.right = checked[3]?1:0;
//                    }
//                });
//                break;
//            case R.id.rl_imu_layout:
//                replaceImuFragment(machine.imu, getString(R.string.imu));
//                break;
//            case R.id.rl_transmitters_layout:
//                replaceTransmitterFragment(machine.transmitters, getString(R.string.ultra_transmitter));
//                break;
//        }
//    }
//
//    private void fillContent() {
//        if(machine!=null){
//            leftWheelDiameter.setText(String.valueOf(machine.axis_x.length));
//            rightWheelDiameter.setText(String.valueOf(machine.axis_y.length));
//            axialLength.setText(String.valueOf(machine.axis_z.length));
//            String[] items = getResources().getStringArray(R.array.turn_direction_type);
//            turnDirection.setText(machine.type==0?items[0]:items[1]);
//            StringBuilder values = new StringBuilder();
//            items = getResources().getStringArray(R.array.obstacle_sensor_type);
//            if(machine.obstacle_sensor.forward == 1) values.append(items[0]).append(" ");
//            if(machine.obstacle_sensor.backward == 1) values.append(items[1]).append(" ");
//            if(machine.obstacle_sensor.left == 1) values.append(items[2]).append(" ");
//            if(machine.obstacle_sensor.right == 1) values.append(items[3]).append(" ");
//            obstacleSensor.setText(values.toString());
//        }
//    }
//
//    private void replaceImuFragment(IMU imu, String title){
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        ModifyImuFragment fragment = ModifyImuFragment.getInstance(imu);
//        transaction.replace(R.id.fl_container, fragment);
//        transaction.addToBackStack(fragment.getClass().getSimpleName());
//        transaction.commit();
//        toolbar.getMenu().clear();
//        toolbar.setTitle(title);
//        toolbar.setNavigationIcon(R.drawable.arrow_back);
//    }
//
//    private void replaceTransmitterFragment(List<Transmitter> transmitters, String title){
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        ModifyTransmitterFragment fragment = ModifyTransmitterFragment.getInstance(transmitters);
//        transaction.replace(R.id.fl_container, fragment);
//        transaction.addToBackStack(fragment.getClass().getSimpleName());
//        transaction.commit();
//        toolbar.getMenu().clear();
//        toolbar.setTitle(title);
//        toolbar.setNavigationIcon(R.drawable.arrow_back);
//    }
//
//    private void showModifyDialog(int titleId, View view, DialogInterface.OnClickListener confirmListener){
//        new AlertDialog.Builder(context).setTitle(titleId).setView(view).setPositiveButton(R.string.confirm, confirmListener).setNegativeButton(R.string.cancel, null).show();
//    }
//
//    private void showSingleChoiceDialog(int titleId, String[] items, DialogInterface.OnClickListener listener){
//        new AlertDialog.Builder(context).setTitle(titleId).setItems(items, listener).setNegativeButton(R.string.cancel, null).show();
//    }
//
//    private void showMultiChoiceDialog(int titleId, String[] items, boolean[] checkedItems, DialogInterface.OnMultiChoiceClickListener listener){
//        new AlertDialog.Builder(context).setTitle(titleId).setMultiChoiceItems(items, checkedItems, listener).setPositiveButton(R.string.confirm, null).show();
//    }
//
//    @Override
//    public void onDestroyView() {
//        ButterKnife.reset(this);
//        super.onDestroyView();
//    }
//
//}
