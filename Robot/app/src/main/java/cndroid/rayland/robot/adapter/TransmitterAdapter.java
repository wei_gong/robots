package cndroid.rayland.robot.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import cn.rayland.api.Transmitter;
import cn.rayland.robot.R;

/**
 * Created by gw on 2016/10/26.
 */

public class TransmitterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Transmitter> datas;
    private static final int ITEM_TYPE_HEADER = 0;
    private static final int ITEM_TYPE_CONTENT = 1;
    private static final int ITEM_TYPE_BOTTOM = 2;
    private int headerCount = 0;
    private int bottomCount = 1;
    private OnClickItemListener onClickItemListener;

    public interface OnClickItemListener{
        void onClickItem(View view, int position);
    }

    public TransmitterAdapter(Context context, List<Transmitter> datas){
        this.context = context;
        this.datas = datas;
    }

    public void setOnClickItemListener(OnClickItemListener onClickItemListener){
        this.onClickItemListener = onClickItemListener;
    }

    public boolean isHeaderView(int position){
        return position < headerCount;
    }

    public boolean isBottomView(int position){
        return position >= headerCount + datas.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(isHeaderView(position)){
            return ITEM_TYPE_HEADER;
        }
        if(isBottomView(position)){
            return ITEM_TYPE_BOTTOM;
        }
        return ITEM_TYPE_CONTENT;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case ITEM_TYPE_HEADER:
                return  new HeaderViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_header_transmitter, parent,false));
            case ITEM_TYPE_BOTTOM:
                return  new BottomViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_bottom_transmitter, parent,false));
            case ITEM_TYPE_CONTENT:
                return  new ContentViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_content_transmitter, parent,false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof ContentViewHolder){
            ((ContentViewHolder) holder).tvId.setText(String.valueOf(datas.get(position).id));
            ((ContentViewHolder) holder).tvOffset.setText(String.valueOf(datas.get(position).offset));
            ((ContentViewHolder) holder).tvCoordinate.setText(datas.get(position).x+","+datas.get(position).y+","+datas.get(position).z);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onClickItemListener!=null){
                        onClickItemListener.onClickItem(v, holder.getAdapterPosition()-headerCount);
                    }
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    new AlertDialog.Builder(context).setMessage(R.string.confirm_to_delete).setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            datas.remove(holder.getAdapterPosition()-headerCount);
                            notifyItemRemoved(holder.getAdapterPosition()-headerCount);
                        }
                    }).setNegativeButton(R.string.cancel, null).show();
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return headerCount+bottomCount+datas.size();
    }

    private class ContentViewHolder extends RecyclerView.ViewHolder{
        TextView tvId;
        TextView tvOffset;
        TextView tvCoordinate;
        public ContentViewHolder(View itemView) {
            super(itemView);
            tvId = (TextView) itemView.findViewById(R.id.tv_id);
            tvOffset = (TextView) itemView.findViewById(R.id.tv_offset);
            tvCoordinate = (TextView) itemView.findViewById(R.id.tv_coordinate);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder{
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    private class BottomViewHolder extends RecyclerView.ViewHolder{
        public BottomViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    datas.add(new Transmitter());
                    notifyItemInserted(headerCount+datas.size());
                }
            });
        }
    }
}
