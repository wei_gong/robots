package cndroid.rayland.robot.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import cn.rayland.bean.MachineTask;
import cn.rayland.bean.RobotState;
import cn.rayland.commands.QueryCommand;
import cn.rayland.commands.X3gCommand;
import cn.rayland.robot.R;
import cndroid.rayland.robot.App;
import cndroid.rayland.robot.utils.ToastUtils;
import cndroid.rayland.robot.view.LineChartView;

/**
 * Created by gwlab on 2016/5/14.
 */
public class DebugFragment extends BaseFragment{

    @InjectView(R.id.et_left_number_turns)
    EditText et_leftNumberTurns;
    @InjectView(R.id.et_right_number_turns)
    EditText et_rightNumberTurns;
    @InjectView(R.id.et_speed_l)
    EditText et_speed_l;
    @InjectView(R.id.et_speed_r)
    EditText et_speed_r;

    @InjectView(R.id.et_yaw)
    EditText etYaw;
    @InjectView(R.id.et_yaw_threshold)
    EditText yawThreshold;
    @InjectView(R.id.et_distance_threshold)
    EditText distanceThreshold;
    @InjectView(R.id.et_forward_distance)
    EditText forwardDistance;
    @InjectView(R.id.et_turn_speed)
    EditText turnSpeed;
    @InjectView(R.id.et_forward_speed)
    EditText forwardSpeed;
    private LineChartView chartView;

    private MachineTask<String> task = new MachineTask<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug, container, false);
        ButterKnife.inject(this, view);
        chartView = (LineChartView) getActivity().findViewById(R.id.LineChartView);
        return view;
    }

    @OnClick({R.id.btn_go, R.id.btn_stop_go, R.id.btn_pilot, R.id.btn_stop_pilot})
    void onClick(View view){
        switch (view.getId()){
            case R.id.btn_go:
                sendTask();
                break;
            case R.id.btn_pilot:
                sendPilotTask();
                break;
            case R.id.btn_stop_go:
            case R.id.btn_stop_pilot:
                App.manager.stop(true);
                break;
        }
    }

    private void sendTask() {
        String left = et_leftNumberTurns.getText().toString().trim();
        String right = et_rightNumberTurns.getText().toString().trim();
        String speedL = et_speed_l.getText().toString().trim();
        String speedR = et_speed_r.getText().toString().trim();
        if(left.equals("")){
           ToastUtils.showShort(context, "请填写左轮圈数");
            et_leftNumberTurns.requestFocus();
            return;
        }
        if(right.equals("")){
            ToastUtils.showShort(context, "请填写右轮圈数");
            et_rightNumberTurns.requestFocus();
            return;
        }
        if(speedL.equals("")){
            ToastUtils.showShort(context, "请填写左轮速度");
            et_speed_l.requestFocus();
            return;
        }
        if(speedR.equals("")){
            ToastUtils.showShort(context, "请填写右轮速度");
            et_speed_r.requestFocus();
            return;
        }
        task.setContent("G7 X" + left + " Y" + right + " A" + speedL + " B" + speedR);
        App.manager.sendTask(task, true);
    }

    private void sendPilotTask() {
        float[] target = chartView.getTarget();
        String yaw = etYaw.getText().toString().trim();
        String yaw_threshold = yawThreshold.getText().toString().trim();
        String distance_threshold = distanceThreshold.getText().toString().trim();
        String forward_distance = forwardDistance.getText().toString().trim();
        String turn_speed = turnSpeed.getText().toString().trim();
        String forward_speed = forwardSpeed.getText().toString().trim();
        if(yaw.equals("")){
            ToastUtils.showShort(context, "请填写朝向角");
            etYaw.requestFocus();
            return;
        }
        if(yaw_threshold.equals("")){
            ToastUtils.showShort(context, "请填写角度偏差");
            yawThreshold.requestFocus();
            return;
        }
        if(distance_threshold.equals("")){
            ToastUtils.showShort(context, "请填写距离偏差");
            distanceThreshold.requestFocus();
            return;
        }
        if(forward_distance.equals("")){
            ToastUtils.showShort(context, "请填写直行距离");
            forwardDistance.requestFocus();
            return;
        }
        if(forward_speed.equals("")){
            ToastUtils.showShort(context, "请填写直行速度");
            forwardSpeed.requestFocus();
            return;
        }
        if(turn_speed.equals("")){
            ToastUtils.showShort(context, "请填写转向速度");
            turnSpeed.requestFocus();
            return;
        }
        task.setContent("G9 X"+target[0]+" Y"+target[1]+" Z0 W"+yaw+" E"+yaw_threshold+" F"+forward_distance+" R"+turn_speed+" S"+forward_speed);
        task.setCallback(new MachineTask.TaskCallback() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onWorking() {
                    }

                    @Override
                    public void onError() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtils.showShort(getActivity(), "Error!!!");
                            }
                        });
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onFinished() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ToastUtils.showShort(getActivity(), "Already Arrived!!!");
                            }
                        });
            }
        });
        App.manager.sendPilotTask(task,Float.parseFloat(distance_threshold));
    }
}
