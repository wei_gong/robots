package cndroid.rayland.robot.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.alibaba.fastjson.JSON;
import butterknife.ButterKnife;
import butterknife.InjectView;
import cn.rayland.bean.RobotState;
import cn.rayland.robot.R;
import cndroid.rayland.robot.App;
import cndroid.rayland.robot.fragment.BaseFragment;
//import cndroid.rayland.robot.fragment.ConfigFragment;
import cndroid.rayland.robot.fragment.MonitorFragment;
import cndroid.rayland.robot.fragment.DebugFragment;
import cndroid.rayland.robot.service.SocketMsgService;
import cndroid.rayland.robot.utils.WebSocketUtil;
import cndroid.rayland.robot.view.LineChartView;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.drawer_layout)
    DrawerLayout drawer;
    @InjectView(R.id.nav_view)
    NavigationView navigationView;
    @InjectView(R.id.LineChartView)
    LineChartView chartView;

    private Thread updateLocation;
    private volatile boolean keepUpdating;
    private DebugFragment debugFragment;
    private MonitorFragment monitorFragment;
//    private ConfigFragment configFragment;
    private int mCurrentFragmentTag;
    private BaseFragment curInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        navigationView.setNavigationItemSelectedListener(this);
        initToolbar();
        updateLocation = new Thread() {
            @Override
            public void run() {
                while(keepUpdating){
                    RobotState state = App.manager.getRobotState();
                    chartView.setMove(state.getX(), state.getY(), state.getYaw());
                    if (WebSocketUtil.remoteImei != null) {
                        WebSocketUtil.sendMessage("send_coordinat" + JSON.toJSONString(state));
                    }
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        };
        keepUpdating = true;
        updateLocation.start();
        setShowFragment(R.id.status_monitor);
    }

    private void initToolbar() {
        toolbar.setTitle(getString(R.string.status_monitor));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.inflateMenu(R.menu.main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId() == R.id.menu_main){
                    if(drawer.isDrawerOpen(GravityCompat.END)){
                        drawer.closeDrawer(GravityCompat.END);
                    }else{
                        drawer.openDrawer(GravityCompat.END);
                    }
                }
                return true;
            }
        });
    }

    /**
     * 设置当前activity所展示的fragment
     *
     * @param tag 侧滑菜单项的id
     */
    private void setShowFragment(int tag) {
        if(mCurrentFragmentTag != 0 && tag == mCurrentFragmentTag){
            return;
        }
        if(tag == R.id.machine_config){
            chartView.setVisibility(View.GONE);
        }else{
            chartView.setVisibility(View.VISIBLE);
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if(curInfoFragment != null){
            ft.hide(curInfoFragment);
        }
        BaseFragment fragment = (BaseFragment) fm.findFragmentByTag(String.valueOf(tag));
        if(fragment != null){
            ft.show(fragment);
        }else{
            switch (tag) {
                case R.id.debug:
                    if (null == debugFragment) {
                        debugFragment = new DebugFragment();
                    }
                    fragment = debugFragment;
                    break;
                case R.id.status_monitor:
                    if (null == monitorFragment) {
                        monitorFragment = new MonitorFragment();
                    }
                    fragment = monitorFragment;
                    break;
                case R.id.machine_config:
//                    if (null == configFragment) {
//                        configFragment = new ConfigFragment();
//                    }
//                    fragment = configFragment;
                    break;
            }
            if (fragment != null) {
                if (!fragment.isAdded()) {
                    ft.add(R.id.container, fragment, String.valueOf(tag));
                } else {
                    ft.show(fragment);
                }
            }
        }
        curInfoFragment = fragment;
        mCurrentFragmentTag = tag;
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopService(new Intent(new Intent(this, SocketMsgService.class)));
        App.manager.stop(true);
        keepUpdating = false;
        try {
            updateLocation.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        drawer.closeDrawer(GravityCompat.END);
        int id = item.getItemId();
        setShowFragment(id);
        toolbar.setTitle(item.getTitle());
        return true;
    }


}
