//package cndroid.rayland.robot.fragment;
//
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Looper;
//import android.support.annotation.Nullable;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import com.alibaba.fastjson.JSON;
//import java.io.File;
//import java.io.FileInputStream;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.Executor;
//import java.util.concurrent.Executors;
//import cn.rayland.api.Machine;
//import cn.rayland.bean.SeniorSetting;
//import cn.rayland.robot.R;
//import cndroid.rayland.robot.App;
//import cndroid.rayland.robot.utils.ToastUtils;
//import viewer.util.IOUtils;
//
///**
// * Created by gw on 2016/9/23.
// */
//
//public class ConfigFragment extends BaseFragment {
//
//    private Executor executor = Executors.newSingleThreadExecutor();
//    private List<SeniorSetting> mDatas = new ArrayList<>();
//    private Machine machine;
//    private Handler handler = new Handler(Looper.getMainLooper());
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_config, container, false);
//        executor.execute(getConfigTask);
//        return view;
//    }
//
//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        if(!hidden){
//            executor.execute(getConfigTask);
//        }else{
//            executor.execute(saveConfigTask);
//        }
//    }
//
//    @Override
//    public void onDestroyView() {
//        executor.execute(saveConfigTask);
//        super.onDestroyView();
//    }
//
//    private Runnable getConfigTask = new Runnable() {
//        @Override
//        public void run() {
//            try {
//                File file = new File(App.CONFIG_PATH);
//                if (!file.exists()) {
//                    byte[] bytes = IOUtils.readToBytes(context.getAssets().open(App.CONFIG_FILE_NAME));
//                    IOUtils.save2File(bytes, App.CONFIG_DIR, App.CONFIG_FILE_NAME);
//                }
//                String machineStr = IOUtils.readToString(new FileInputStream(file));
//                mDatas.addAll(JSON.parseArray(machineStr, SeniorSetting.class));
//                for(SeniorSetting setting : mDatas){
//                    if(setting.isSelected()){
//                        machine = setting.getSetting();
//                        handler.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                addFragment();
//                            }
//                        });
//                        break;
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        ToastUtils.showShort(context, getText(R.string.printer_configuration_file_read_failure));
//                    }
//                });
//            }
//        }
//    };
//
//    private Runnable saveConfigTask = new Runnable() {
//        @Override
//        public void run() {
//            if(mDatas != null){
//                try {
//                    IOUtils.save2File(JSON.toJSONString(mDatas).getBytes(),App.CONFIG_DIR,App.CONFIG_FILE_NAME);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    };
//
//    private void addFragment(){
//        FragmentManager fragmentManager = getFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        ModifyMachineFragment fragment = ModifyMachineFragment.getInstance(machine);
//        transaction.replace(R.id.fl_container, fragment);
//        transaction.commit();
//    }
//
//}
