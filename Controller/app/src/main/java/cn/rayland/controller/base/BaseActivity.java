package cn.rayland.controller.base;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import butterknife.ButterKnife;

/**
 * Created by gw on 2016/5/26.
 */
public abstract class BaseActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(getContentViewId());
        ButterKnife.inject(this);
        if(getIntent()!=null){
            handleIntent(getIntent());
        }
        init(savedInstanceState);
    }

    protected void handleIntent(Intent intent) {}
    protected void init(Bundle savedInstanceState) {}
    protected abstract int getContentViewId();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

}
