package cn.rayland.controller;


import android.os.Bundle;

import cn.rayland.controller.base.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        ZMQManager.init();
        getSupportFragmentManager().beginTransaction().add(R.id.activity_main, new PrinterHeaderFragment()).commit();
    }
}
