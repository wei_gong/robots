package cn.rayland.controller;

import android.os.Bundle;
import android.view.View;

import butterknife.InjectView;
import cn.rayland.controller.base.BaseFragment;
import cn.rayland.controller.view.RockerView;

/**
 * Created by gw on 2016/2/3.
 */
public class PrinterHeaderFragment extends BaseFragment {
    @InjectView(R.id.id_rocker)
    RockerView rockerView;
    private short speed = 20;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_printer_header_control;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        initEvent();
    }

    private void initEvent() {
        rockerView.setOnDirChangeListener(new RockerView.OnDirChangeListener() {
            @Override
            public void onDirChange(int oldDir, int dir) {
//                ZMQManager.sendFeedback(shortToBytes((short)0, (short)0));
                switch (dir) {
                    case RockerView.UP:
                        ZMQManager.sendControl(shortToBytes(speed, speed));
                        break;
                    case RockerView.DOWN:
                        ZMQManager.sendControl(shortToBytes((short)(-speed),(short)(-speed)));
                        break;
                    case RockerView.LEFT:
                        ZMQManager.sendControl(shortToBytes((short) (-1 * speed),speed));
                        break;
                    case RockerView.RIGHT:
                        ZMQManager.sendControl(shortToBytes(speed,(short) (-1 *speed)));
                        break;
                    case RockerView.CENTER:
                        ZMQManager.sendControl(shortToBytes((short)0, (short)0));
                        break;
                }
            }

            @Override
            public void onTricky() {
            }
        });
    }

    public static byte[] shortToBytes(short l, short r){
        byte[] b = new byte[4];
        b[0] = (byte)(l);
        b[1] = (byte)(l >> 8);
        b[2] = (byte)(r);
        b[3] = (byte)(r >> 8);
        return b;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ZMQManager.destory();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            int j = 1;
            for(int i=0;i<100;i++){
                ZMQManager.sendControl(shortToBytes((short)(j*speed), (short)(j*speed)));
                j *= -1;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
