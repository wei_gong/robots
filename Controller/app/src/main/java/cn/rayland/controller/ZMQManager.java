package cn.rayland.controller;

import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import android.os.Handler;
import android.os.HandlerThread;

import java.io.DataOutputStream;
import java.io.IOException;

public class ZMQManager {

	private static Socket sendSocket;
	private static ZContext context;
	private static HandlerThread sendThread;
	private static Handler sendHandler;
	
	public static void init(){
		context = new ZContext();
		DataOutputStream dos = null;
		Process process = null;
		try {
			process = Runtime.getRuntime().exec("su\n");
			dos = new DataOutputStream(process.getOutputStream());
			dos.writeBytes("ifconfig eth0 192.168.199.3 netmask 255.255.255.0\n");
			dos.writeBytes("exit\n");
			dos.flush();
			process.waitFor();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		} finally {
			if(dos!=null){
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(process!=null){
				process.destroy();
			}
		}
		sendSocket = context.createSocket(ZMQ.PUB);
		sendSocket.bind("tcp://*:5553");
		sendThread = new HandlerThread("send");
		sendThread.start();
		sendHandler = new Handler(sendThread.getLooper());
	}
	

	
	public static void sendControl(final byte[] bytes){
		sendHandler.post(new Runnable() {
			public void run() {
				sendSocket.send(bytes, bytes.length);
			}
		});
	}
	
	public static void destory(){

		if(sendThread!=null){
			sendThread.quit();
			sendThread = null;
		}

		if(sendSocket!=null){
			sendSocket.close();
			sendSocket = null;
		}
		if(context!=null){
			context = null;
		}
	}
	
}
